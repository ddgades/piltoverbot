﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Data.Mongo.Entity;
using Data.Mongo.Repository.Abstracts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PiltoverBot;

namespace UnitTests
{
    [TestClass]
    public class MongoTests
    {
        [TestMethod]
        public async Task TestMethod1()
        {
            var services = Bootstrapper.ConfigureServices();
            
            var repo = services.GetService<MongoRepository>();
            var testname = "test" + DateTime.Now;
            var guild = new Guild()
            {
                Name = testname,
                GuildId = 123

            };
            if (repo == null)
            {
                Assert.Fail();
            }
            await repo.AddManyAsync(new[] { guild });

            var guilds = await repo.GetManyAsync<Guild>(x=>x.Name == testname, CancellationToken.None);

            Assert.IsTrue(guilds.Any(x=>x.GuildId == 123));
        }
    }
}
