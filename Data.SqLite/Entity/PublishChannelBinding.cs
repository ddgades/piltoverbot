﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Data.SqLite.Repositories.Abstracts;

namespace Data.SqLite.Entity
{
    public class PublishChannelBinding:IBaseEntity, INamedEntity, IGuildRelatedEntity
    {
        [Key]
        public ulong Id { get; set; }

        public string Name { get; set; }
        
        public ulong ApprovalChannelId { get; set; }

        public ulong PublishChannelId { get; set; }

        public ulong GuildId { get; set; }

        public ulong PublishTypeId { get; set; }

        [ForeignKey("PublishTypeId")]
        public virtual PublishType PublishType { get; set; }

        [ForeignKey("GuildId")]
        public virtual VirtualGuild Guild { get; set; }
    }
}