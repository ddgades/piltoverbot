﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Data.SqLite.Repositories.Abstracts;

namespace Data.SqLite.Entity
{
    public class BulletinBoardConfiguration:IBaseEntity,IGuildRelatedEntity
    {
        [Key]
        public ulong Id { get; set; }

        public ulong CategoryId { get; set; }

        public ulong HostRoleId { get; set; }

        public ulong BulletinBoardChannelId { get; set; }

        public ulong DefaultChannelId { get; set; }

        public ulong GuildId { get; set; }

        [ForeignKey("GuildId")]
        public virtual VirtualGuild Guild { get; set; }
    }
}