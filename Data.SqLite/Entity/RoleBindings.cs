﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Data.SqLite.Repositories.Abstracts;

namespace Data.SqLite.Entity
{
    public class RoleBinding:IBaseEntity, INamedEntity, IGuildRelatedEntity
    {
        [Key]
        public ulong Id { get; set; }

        public string Name { get; set; }

        public ulong RoleId { get; set; }

        public ulong EmojiId { get; set; }
        
        public ulong StackId { get; set; }

        [ForeignKey("StackId")]
        public virtual RoleStack Stack { get; set; }

        public ulong GuildId { get; set; }

        [ForeignKey("GuildId")]
        public virtual VirtualGuild Guild { get; set; }
    }
}