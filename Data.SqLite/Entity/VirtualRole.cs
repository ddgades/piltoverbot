﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Data.SqLite.Repositories.Abstracts;

namespace Data.SqLite.Entity
{
    public class VirtualRole : IBaseEntity, INamedEntity, IMemberRelatedEntity
    {
        [Key]
        public ulong Id { get; set; }

        public string Name { get; set; }

        public ulong RoleBindingId { get; set; }

        [ForeignKey("RoleBindingId")]
        public virtual RoleBinding RoleBinding { get; set; }

        public ulong MemberId { get; set; }

        [ForeignKey("MemberId")]
        public virtual Member Member { get; set; }
    }
}