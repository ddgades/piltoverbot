﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Data.SqLite.Repositories.Abstracts;

namespace Data.SqLite.Entity
{
    public class RoleStack:IBaseEntity, INamedEntity, IGuildRelatedEntity
    {
        [Key]
        public ulong Id { get; set; }

        public string Name { get; set; }
        
        public ulong GuildId { get; set; }

        [ForeignKey("GuildId")]
        public virtual VirtualGuild Guild { get; set; }
    }
}