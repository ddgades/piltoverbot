﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Data.SqLite.Repositories.Abstracts;

namespace Data.SqLite.Entity
{
    public class VirtualGuild:IBaseEntity, INamedEntity, IGuildRelatedEntity
    {
        [Key]
        public ulong Id { get; set; }

        public string Name { get; set; }

        public ulong GuildId { get; set; }

        public virtual BulletinBoardConfiguration BulletinBoardConfiguration { get; set; }

        public virtual List<Member> Members { get; set; }
        public virtual List<PublishChannelBinding> ChannelBindings { get; set; }
        public virtual List<PublishType> PublishTypes { get; set; }
        public virtual List<RoleStack> RoleStacks { get; set; }
        public virtual List<RoleBinding> RoleBindings { get; set; }
    }
}