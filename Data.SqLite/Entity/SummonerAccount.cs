﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Data.SqLite.Repositories.Abstracts;

namespace Data.SqLite.Entity
{
    public class SummonerAccount:IBaseEntity, INamedEntity
    {
        [Key]
        public ulong Id { get; set; }

        public bool IsConfirmed { get; set; }

        public string Name { get; set; }

        public string SummonerId { get; set; }

        public string Puuid { get; set; }

        public string ConfirmationCode { get; set; }

        public ulong MemberId { get; set; }

        [ForeignKey("MemberId")]
        public virtual Member Member { get; set; }
    }
}