﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Data.SqLite.Repositories.Abstracts;

namespace Data.SqLite.Entity
{
    public class Member:IBaseEntity, INamedEntity,IGuildRelatedEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public ulong Id { get; set; }

        public string Name { get; set; }

        public virtual List<VirtualRole> Roles { get; set; }

        public ulong GuildId { get; set; }

        [ForeignKey("GuildId")]
        public virtual VirtualGuild Guild { get; set; }

        public virtual List<SummonerAccount> SummonerAccounts { get; set; }
    }
}