﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class _230620211 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BulletinBoardConfigurations",
                columns: table => new
                {
                    Id = table.Column<ulong>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CategoryId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    HostRoleId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    BulletinBoardChannelId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    DefaultChannelId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    GuildId = table.Column<ulong>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BulletinBoardConfigurations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BulletinBoardConfigurations_Guilds_GuildId",
                        column: x => x.GuildId,
                        principalTable: "Guilds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BulletinBoardConfigurations_GuildId",
                table: "BulletinBoardConfigurations",
                column: "GuildId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BulletinBoardConfigurations");
        }
    }
}
