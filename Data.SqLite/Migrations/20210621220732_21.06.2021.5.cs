﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class _210620215 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VirtualRoles_Members_MemberId1",
                table: "VirtualRoles");

            migrationBuilder.DropForeignKey(
                name: "FK_VirtualRoles_RoleBindings_MemberId",
                table: "VirtualRoles");

            migrationBuilder.DropIndex(
                name: "IX_VirtualRoles_MemberId1",
                table: "VirtualRoles");

            migrationBuilder.DropColumn(
                name: "MemberId1",
                table: "VirtualRoles");

            migrationBuilder.AddForeignKey(
                name: "FK_VirtualRoles_Members_MemberId",
                table: "VirtualRoles",
                column: "MemberId",
                principalTable: "Members",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VirtualRoles_Members_MemberId",
                table: "VirtualRoles");

            migrationBuilder.AddColumn<ulong>(
                name: "MemberId1",
                table: "VirtualRoles",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0ul);

            migrationBuilder.CreateIndex(
                name: "IX_VirtualRoles_MemberId1",
                table: "VirtualRoles",
                column: "MemberId1");

            migrationBuilder.AddForeignKey(
                name: "FK_VirtualRoles_Members_MemberId1",
                table: "VirtualRoles",
                column: "MemberId1",
                principalTable: "Members",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VirtualRoles_RoleBindings_MemberId",
                table: "VirtualRoles",
                column: "MemberId",
                principalTable: "RoleBindings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
