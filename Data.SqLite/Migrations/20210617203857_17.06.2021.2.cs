﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class _170620212 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Members_Fractions_FractionId",
                table: "Members");

            migrationBuilder.DropIndex(
                name: "IX_Members_FractionId",
                table: "Members");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Members_FractionId",
                table: "Members",
                column: "FractionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Members_Fractions_FractionId",
                table: "Members",
                column: "FractionId",
                principalTable: "Fractions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
