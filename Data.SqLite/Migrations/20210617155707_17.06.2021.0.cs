﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class _170620210 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SummonerAccounts_MemberId",
                table: "SummonerAccounts");

            migrationBuilder.DropIndex(
                name: "IX_GameLobbies_MemberId",
                table: "GameLobbies");

            migrationBuilder.DropColumn(
                name: "ConfirmationCode",
                table: "GameLobbies");

            migrationBuilder.DropColumn(
                name: "Puuid",
                table: "GameLobbies");

            migrationBuilder.RenameColumn(
                name: "MaxMembers",
                table: "SummonerAccounts",
                newName: "SummonerName");

            migrationBuilder.RenameColumn(
                name: "IsActive",
                table: "SummonerAccounts",
                newName: "IsConfirmed");

            migrationBuilder.RenameColumn(
                name: "SummonerName",
                table: "GameLobbies",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "SummonerId",
                table: "GameLobbies",
                newName: "MaxMembers");

            migrationBuilder.RenameColumn(
                name: "IsConfirmed",
                table: "GameLobbies",
                newName: "IsActive");

            migrationBuilder.AddColumn<string>(
                name: "ConfirmationCode",
                table: "SummonerAccounts",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Puuid",
                table: "SummonerAccounts",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SummonerId",
                table: "SummonerAccounts",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<ulong>(
                name: "GuildId",
                table: "Members",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0ul);

            migrationBuilder.CreateTable(
                name: "Guilds",
                columns: table => new
                {
                    Id = table.Column<ulong>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    DefaultChannelId = table.Column<ulong>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Guilds", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PublishTypes",
                columns: table => new
                {
                    Id = table.Column<ulong>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    GuildId = table.Column<ulong>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PublishTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PublishTypes_Guilds_GuildId",
                        column: x => x.GuildId,
                        principalTable: "Guilds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PublishChannelBindings",
                columns: table => new
                {
                    Id = table.Column<ulong>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    ApprovalChannelId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    PublishChannelId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    GuildId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    PublishTypeId = table.Column<ulong>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PublishChannelBindings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PublishChannelBindings_Guilds_GuildId",
                        column: x => x.GuildId,
                        principalTable: "Guilds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PublishChannelBindings_PublishTypes_PublishTypeId",
                        column: x => x.PublishTypeId,
                        principalTable: "PublishTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SummonerAccounts_MemberId",
                table: "SummonerAccounts",
                column: "MemberId");

            migrationBuilder.CreateIndex(
                name: "IX_Members_GuildId",
                table: "Members",
                column: "GuildId");

            migrationBuilder.CreateIndex(
                name: "IX_GameLobbies_MemberId",
                table: "GameLobbies",
                column: "MemberId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PublishChannelBindings_GuildId",
                table: "PublishChannelBindings",
                column: "GuildId");

            migrationBuilder.CreateIndex(
                name: "IX_PublishChannelBindings_PublishTypeId",
                table: "PublishChannelBindings",
                column: "PublishTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PublishTypes_GuildId",
                table: "PublishTypes",
                column: "GuildId");

            migrationBuilder.AddForeignKey(
                name: "FK_Members_Guilds_GuildId",
                table: "Members",
                column: "GuildId",
                principalTable: "Guilds",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Members_Guilds_GuildId",
                table: "Members");

            migrationBuilder.DropTable(
                name: "PublishChannelBindings");

            migrationBuilder.DropTable(
                name: "PublishTypes");

            migrationBuilder.DropTable(
                name: "Guilds");

            migrationBuilder.DropIndex(
                name: "IX_SummonerAccounts_MemberId",
                table: "SummonerAccounts");

            migrationBuilder.DropIndex(
                name: "IX_Members_GuildId",
                table: "Members");

            migrationBuilder.DropIndex(
                name: "IX_GameLobbies_MemberId",
                table: "GameLobbies");

            migrationBuilder.DropColumn(
                name: "ConfirmationCode",
                table: "SummonerAccounts");

            migrationBuilder.DropColumn(
                name: "Puuid",
                table: "SummonerAccounts");

            migrationBuilder.DropColumn(
                name: "SummonerId",
                table: "SummonerAccounts");

            migrationBuilder.DropColumn(
                name: "GuildId",
                table: "Members");

            migrationBuilder.RenameColumn(
                name: "SummonerName",
                table: "SummonerAccounts",
                newName: "MaxMembers");

            migrationBuilder.RenameColumn(
                name: "IsConfirmed",
                table: "SummonerAccounts",
                newName: "IsActive");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "GameLobbies",
                newName: "SummonerName");

            migrationBuilder.RenameColumn(
                name: "MaxMembers",
                table: "GameLobbies",
                newName: "SummonerId");

            migrationBuilder.RenameColumn(
                name: "IsActive",
                table: "GameLobbies",
                newName: "IsConfirmed");

            migrationBuilder.AddColumn<string>(
                name: "ConfirmationCode",
                table: "GameLobbies",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Puuid",
                table: "GameLobbies",
                type: "TEXT",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SummonerAccounts_MemberId",
                table: "SummonerAccounts",
                column: "MemberId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GameLobbies_MemberId",
                table: "GameLobbies",
                column: "MemberId");
        }
    }
}
