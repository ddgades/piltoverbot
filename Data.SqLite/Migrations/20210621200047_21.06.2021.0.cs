﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class _210620210 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Fractions");

            migrationBuilder.DropColumn(
                name: "GuildId",
                table: "Guilds");

            migrationBuilder.AlterColumn<ulong>(
                name: "Id",
                table: "Members",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(ulong),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.CreateTable(
                name: "RoleStacks",
                columns: table => new
                {
                    Id = table.Column<ulong>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    GuildId = table.Column<ulong>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleStacks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoleStacks_Guilds_GuildId",
                        column: x => x.GuildId,
                        principalTable: "Guilds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RoleBindings",
                columns: table => new
                {
                    Id = table.Column<ulong>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    RoleId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    EmojiId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    StackId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    GuildId = table.Column<ulong>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleBindings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoleBindings_Guilds_GuildId",
                        column: x => x.GuildId,
                        principalTable: "Guilds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RoleBindings_RoleStacks_StackId",
                        column: x => x.StackId,
                        principalTable: "RoleStacks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VirtualRoles",
                columns: table => new
                {
                    Id = table.Column<ulong>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    RoleBindingId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    MemberId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    MemberId1 = table.Column<ulong>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VirtualRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VirtualRoles_Members_MemberId1",
                        column: x => x.MemberId1,
                        principalTable: "Members",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VirtualRoles_RoleBindings_MemberId",
                        column: x => x.MemberId,
                        principalTable: "RoleBindings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VirtualRoles_RoleBindings_RoleBindingId",
                        column: x => x.RoleBindingId,
                        principalTable: "RoleBindings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RoleBindings_GuildId",
                table: "RoleBindings",
                column: "GuildId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleBindings_StackId",
                table: "RoleBindings",
                column: "StackId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleStacks_GuildId",
                table: "RoleStacks",
                column: "GuildId");

            migrationBuilder.CreateIndex(
                name: "IX_VirtualRoles_MemberId",
                table: "VirtualRoles",
                column: "MemberId");

            migrationBuilder.CreateIndex(
                name: "IX_VirtualRoles_MemberId1",
                table: "VirtualRoles",
                column: "MemberId1");

            migrationBuilder.CreateIndex(
                name: "IX_VirtualRoles_RoleBindingId",
                table: "VirtualRoles",
                column: "RoleBindingId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VirtualRoles");

            migrationBuilder.DropTable(
                name: "RoleBindings");

            migrationBuilder.DropTable(
                name: "RoleStacks");

            migrationBuilder.AlterColumn<ulong>(
                name: "Id",
                table: "Members",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(ulong),
                oldType: "INTEGER")
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<ulong>(
                name: "GuildId",
                table: "Guilds",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0ul);

            migrationBuilder.CreateTable(
                name: "Fractions",
                columns: table => new
                {
                    Id = table.Column<ulong>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    GuildId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    IsActive = table.Column<bool>(type: "INTEGER", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    RoleId = table.Column<ulong>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fractions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Fractions_Guilds_GuildId",
                        column: x => x.GuildId,
                        principalTable: "Guilds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Fractions_GuildId",
                table: "Fractions",
                column: "GuildId");
        }
    }
}
