﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class _170620211 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<ulong>(
                name: "FractionId",
                table: "Members",
                type: "INTEGER",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Fractions",
                columns: table => new
                {
                    Id = table.Column<ulong>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    IsActive = table.Column<bool>(type: "INTEGER", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    RoleId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    GuildId = table.Column<ulong>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fractions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Fractions_Guilds_GuildId",
                        column: x => x.GuildId,
                        principalTable: "Guilds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Members_FractionId",
                table: "Members",
                column: "FractionId");

            migrationBuilder.CreateIndex(
                name: "IX_Fractions_GuildId",
                table: "Fractions",
                column: "GuildId");

            migrationBuilder.AddForeignKey(
                name: "FK_Members_Fractions_FractionId",
                table: "Members",
                column: "FractionId",
                principalTable: "Fractions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Members_Fractions_FractionId",
                table: "Members");

            migrationBuilder.DropTable(
                name: "Fractions");

            migrationBuilder.DropIndex(
                name: "IX_Members_FractionId",
                table: "Members");

            migrationBuilder.DropColumn(
                name: "FractionId",
                table: "Members");
        }
    }
}
