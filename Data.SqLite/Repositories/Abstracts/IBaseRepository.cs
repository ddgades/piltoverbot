﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Data.SqLite.Repositories.Abstracts
{
    public interface IRepository<T, V>
        where T : IBaseEntity
        where V : DbContext
    {
        public V DbContext { get; }

        public Task<T> AddAsync(T entity);

        public Task UpdateAsync(T entity);

        public Task RemoveAsync(T entity);
    }


    
}
