﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Data.SqLite.Repositories.Abstracts
{
    public class GenericBaseRepository<T> : IMemberRepository<T>
        where T : class, IBaseEntity
    {
        public GenericBaseRepository(MemberContext dbContext)
        {
            DbContext = dbContext;
        }

        public MemberContext DbContext { get; }

        public async Task<T> AddAsync(T entity)
        {
            var result = await DbContext.Set<T>().AddAsync(entity);
            
            await DbContext.SaveChangesAsync();

            return await DbContext.Set<T>().AsQueryable().FilterById(result.Entity.Id).FirstOrDefaultAsync();
        }

        public async Task RemoveAsync(T entity)
        {
            DbContext.Set<T>().Remove(entity);

            await DbContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            var result =await DbContext.Set<T>().FirstOrDefaultAsync(x=>x.Id == entity.Id);
            if (result == null)
            {
                return;
            }

            DbContext.Set<T>().Update(entity);

            await DbContext.SaveChangesAsync();
        }
    }


    public interface IMemberRepository<T>:IRepository<T,MemberContext>
        where T : IBaseEntity
    {
    }
}