﻿using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Data.SqLite.Repositories.Abstracts
{
    public interface IBaseEntity
    {
        public ulong Id { get; set; }
    }

    public interface IBaseEntity<TKey>
    {
        public TKey Id { get; set; }
    }

    public static class BaseEntityExtension
    {
        public static IQueryable<T> AsQueryable<T,TV>(this IRepository<T,TV> repository)
            where T: class, IBaseEntity
            where TV : DbContext
        {
            return repository.DbContext.Set<T>().AsQueryable();
        }

        public static IQueryable<T> FilterById<T>(this IQueryable<T> data, ulong id)
            where T: class, IBaseEntity
        {
            return data.Where(x => x.Id == id);
        }
    }
}