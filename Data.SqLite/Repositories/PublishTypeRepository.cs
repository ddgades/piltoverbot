﻿using Data.SqLite.Entity;
using Data.SqLite.Repositories.Abstracts;

namespace Data.SqLite.Repositories
{
    public class PublishTypeRepository:GenericBaseRepository<PublishType>
    {
        public PublishTypeRepository(MemberContext dbContext) : base(dbContext)
        {
        }
    }
}