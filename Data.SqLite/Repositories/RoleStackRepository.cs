﻿using Data.SqLite.Entity;
using Data.SqLite.Repositories.Abstracts;

namespace Data.SqLite.Repositories
{
    public class RoleStackRepository:GenericBaseRepository<RoleStack>
    {
        public RoleStackRepository(MemberContext dbContext) : base(dbContext)
        {
        }
    }
}