﻿using Data.SqLite.Entity;
using Data.SqLite.Repositories.Abstracts;

namespace Data.SqLite.Repositories
{
    public class VirtualRoleRepository:GenericBaseRepository<VirtualRole>
    {
        public VirtualRoleRepository(MemberContext dbContext) : base(dbContext)
        {
        }
    }
}