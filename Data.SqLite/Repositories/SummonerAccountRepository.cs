﻿using Data.SqLite.Entity;
using Data.SqLite.Repositories.Abstracts;

namespace Data.SqLite.Repositories
{
    public class SummonerAccountRepository:GenericBaseRepository<SummonerAccount>
    {
        public SummonerAccountRepository(MemberContext dbContext) : base(dbContext)
        {
        }
    }
}