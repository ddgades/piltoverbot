﻿using Data.SqLite.Entity;
using Data.SqLite.Repositories.Abstracts;

namespace Data.SqLite.Repositories
{
    public class RoleBindingRepository:GenericBaseRepository<RoleBinding>
    {
        public RoleBindingRepository(MemberContext dbContext) : base(dbContext)
        {
        }
    }
}