﻿using Data.SqLite.Entity;
using Data.SqLite.Repositories.Abstracts;

namespace Data.SqLite.Repositories
{
    public class PublishChannelBindingRepository:GenericBaseRepository<PublishChannelBinding>
    {
        public PublishChannelBindingRepository(MemberContext dbContext) : base(dbContext)
        {
        }
    }
}