﻿using Data.SqLite.Entity;
using Data.SqLite.Repositories.Abstracts;

namespace Data.SqLite.Repositories
{
    public class BulletinBoardConfigurationRepository:GenericBaseRepository<BulletinBoardConfiguration>
    {
        public BulletinBoardConfigurationRepository(MemberContext dbContext) : base(dbContext)
        {
        }
    }
}