﻿using Data.SqLite.Entity;
using Data.SqLite.Repositories.Abstracts;

namespace Data.SqLite.Repositories
{
    public class MemberRepository:GenericBaseRepository<Member>
    {
        public MemberRepository(MemberContext dbContext) : base(dbContext)
        {
        }
    }
}