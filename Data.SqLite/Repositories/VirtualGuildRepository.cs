﻿using Data.SqLite.Entity;
using Data.SqLite.Repositories.Abstracts;

namespace Data.SqLite.Repositories
{
    public class VirtualGuildRepository:GenericBaseRepository<VirtualGuild>
    {
        public VirtualGuildRepository(MemberContext dbContext) : base(dbContext)
        {
        }
    }
}
