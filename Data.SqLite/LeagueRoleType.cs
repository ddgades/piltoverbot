﻿namespace Data.SqLite
{
    public enum LeagueRoleType
    {
        Top = 0,
        Jungle = 1,
        Mid = 2,
        Bottom = 3,
        Support = 4,
        Flex = 5
    }
}