﻿using Data.SqLite.Entity;
using Microsoft.EntityFrameworkCore;

namespace Data.SqLite
{
    public class MemberContext : DbContext
    {
        public DbSet<VirtualGuild> Guilds { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<SummonerAccount> SummonerAccounts { get; set; }

        public DbSet<PublishType> PublishTypes { get; set; }

        public DbSet<PublishChannelBinding> PublishChannelBindings { get; set; }

        public DbSet<VirtualRole> VirtualRoles { get; set; }

        public DbSet<RoleStack> RoleStacks { get; set; }

        public DbSet<RoleBinding> RoleBindings { get; set; }

        public DbSet<BulletinBoardConfiguration> BulletinBoardConfigurations { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=MemberDB.db;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<VirtualGuild>().ToTable("Guilds");
            modelBuilder.Entity<Member>().ToTable("Members");
            modelBuilder.Entity<SummonerAccount>().ToTable("SummonerAccounts");
            modelBuilder.Entity<PublishChannelBinding>().ToTable("PublishChannelBindings");
            modelBuilder.Entity<PublishType>().ToTable("PublishTypes");
            modelBuilder.Entity<RoleStack>().ToTable("RoleStacks");
            modelBuilder.Entity<RoleBinding>().ToTable("RoleBindings");
            modelBuilder.Entity<VirtualRole>().ToTable("VirtualRoles");
            modelBuilder.Entity<BulletinBoardConfiguration>().ToTable("BulletinBoardConfigurations");
            
        }
    }
}
