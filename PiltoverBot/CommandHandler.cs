﻿using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Enums;
using DSharpPlus.Interactivity.Extensions;
using PiltoverBot.Commands;
using PiltoverBot.Commands.Admin;
using PiltoverBot.Commands.Admin.Roles;

namespace PiltoverBot
{
    public static class CommandHandler
    {
        public static void ConfigureCommands(this DiscordClient discord, IServiceProvider collection)
        {
            var commands = discord.UseCommandsNext(new CommandsNextConfiguration()
            {
                StringPrefixes = new[] { "!" },
                Services = collection,
                EnableDefaultHelp = true,
                UseDefaultCommandHandler = false,

            });

            discord.UseInteractivity(new InteractivityConfiguration()
            {
                PollBehaviour = PollBehaviour.KeepEmojis,
                Timeout = TimeSpan.FromSeconds(30)
            });

            commands.RegisterCommands<AdminCommands>();
            //commands.RegisterCommands<PublishChannelBindingCommands>();
            //commands.RegisterCommands<PublishTypesCommands>();
            //commands.RegisterCommands<PublishCommands>();
            commands.RegisterCommands<AuthenticationCommands>();
            commands.RegisterCommands<GuildConfigurationCommands>();
            //commands.RegisterCommands<RolesCommands>();
            //commands.RegisterCommands<RoleStackCommands>();
            //commands.RegisterCommands<RoleBindingCommands>();

            commands.RegisterCommands<LobbyCommands>();

            discord.MessageCreated += MessageCommandHandler;
        }

        private static Task MessageCommandHandler(DiscordClient client, MessageCreateEventArgs e)
        {
            var cnext = client.GetCommandsNext();
            var msg = e.Message;

            var cmdStart = msg.GetStringPrefixLength("!");
            if (cmdStart == -1) return Task.CompletedTask;

            var prefix = msg.Content.Substring(0, cmdStart);
            var cmdString = msg.Content.Substring(cmdStart);

            var command = cnext.FindCommand(cmdString, out var args);
            if (command == null) return Task.CompletedTask;

            var ctx = cnext.CreateContext(msg, prefix, command, args);
            Task.Run(async () =>
            {
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(ctx.Member.Locale ?? ctx.Guild.PreferredLocale);
                await cnext.ExecuteCommandAsync(ctx);
            });

            return Task.CompletedTask;
        }
    }
}