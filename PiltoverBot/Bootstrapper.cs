﻿using System;
using Data.Mongo;
using DSharpPlus;
using Microsoft.Extensions.DependencyInjection;
using PiltoverBot.Services;
using RiotSharp;

namespace PiltoverBot
{
    public static class Bootstrapper
    {
        public static IServiceProvider ConfigureServices()
        {
            var token = Environment.GetEnvironmentVariable("DiscordToken", EnvironmentVariableTarget.Machine);

            var config = new DiscordConfiguration()
            {
                Intents = DiscordIntents.DirectMessageReactions
                          | DiscordIntents.DirectMessages
                          | DiscordIntents.GuildEmojis
                          | DiscordIntents.GuildInvites
                          | DiscordIntents.GuildMembers
                          | DiscordIntents.GuildMessages
                          | DiscordIntents.Guilds
                          | DiscordIntents.GuildMessageReactions
                          | DiscordIntents.GuildVoiceStates,
                Token = token,
                TokenType = DSharpPlus.TokenType.Bot,
            };

            var discord = new DiscordClient(config);

            //var scoreManager = new ScoreManager();
           
            var map = new ServiceCollection();

            map.AddMongo();
                // Repeat this for all the service classes
                // and other dependencies that your commands might need.
                map.AddSingleton(new RiotService(RiotApi.GetDevelopmentInstance(Constants.ApiKey)))
                //.AddDbContext<MemberContext>()
                .AddSingleton(discord)
                
                .AddSingleton<LobbyManager>();
                //.AddSingleton<IMemberRepository<Member>, MemberRepository>()
                //.AddSingleton<IMemberRepository<SummonerAccount>, SummonerAccountRepository>()
                //.AddSingleton<IMemberRepository<VirtualGuild>, VirtualGuildRepository>()
                //.AddSingleton<IMemberRepository<PublishType>, PublishTypeRepository>()
                //.AddSingleton<IMemberRepository<PublishChannelBinding>, PublishChannelBindingRepository>()
                //.AddSingleton<IMemberRepository<RoleStack>, RoleStackRepository>()
                //.AddSingleton<IMemberRepository<VirtualRole>, VirtualRoleRepository>()
                //.AddSingleton<IMemberRepository<RoleBinding>, RoleBindingRepository>()
                //.AddSingleton<IMemberRepository<BulletinBoardConfiguration>, BulletinBoardConfigurationRepository>();
            
            

            // When all your required services are in the collection, build the container.
            // Tip: There's an overload taking in a 'validateScopes' bool to make sure
            // you haven't made any mistakes in your dependency graph.
            return map.BuildServiceProvider();
        }
    }
}