﻿using System;
using System.Threading.Tasks;
using DSharpPlus;
using Microsoft.Extensions.DependencyInjection;

namespace PiltoverBot
{
    class Program
    {
        static void Main(string[] args)
        {
            new Program().MainAsync().GetAwaiter().GetResult();
        }

        private readonly DiscordClient _discord;

        private readonly IServiceProvider _services;

        private Program()
        {
            // Setup your DI container.
            _services = Bootstrapper.ConfigureServices();

            _discord = _services.GetService<DiscordClient>();

            _discord.ConfigureCommands(_services);
        }

        // If any services require the client, or the CommandService, or something else you keep on hand,
        // pass them as parameters into this method as needed.
        // If this method is getting pretty long, you can seperate it out into another file using partials.
        

        private async Task MainAsync()
        {
            await _discord.ConnectAsync();
            await Task.Delay(-1);
        }
    }
}
