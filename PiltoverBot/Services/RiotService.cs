﻿using System.Threading.Tasks;
using RiotSharp;
using RiotSharp.Endpoints.StatusEndpoint;
using RiotSharp.Endpoints.SummonerEndpoint;
using RiotSharp.Misc;

namespace PiltoverBot.Services
{
    public class RiotService
    {
        private readonly RiotApi _riotApi;
        private readonly Region _region;

        public RiotService(RiotApi riotApi)
        {
            _riotApi = riotApi;
            _region = Region.Ru;
        }

        public async Task<Summoner> GetSummonerAsync(string summonerName)
        {
            var summoner = await _riotApi.Summoner.GetSummonerByNameAsync(_region, summonerName);
            
            return summoner;
        }

        public async Task<string> GetThirdPartyCode(string summonerId)
        {
            try
            {
                var x = await _riotApi.ThirdParty.GetThirdPartyCodeBySummonerIdAsync(Region.Ru, summonerId);
                return x;
            }
            catch
            {
                return string.Empty;
            }
            
        }

        public Task<ShardStatus> GetStatus()
        {
            return _riotApi.Status.GetShardStatusAsync(_region);
        }
    }
}
