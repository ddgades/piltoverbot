﻿using System.Linq;
using System.Threading.Tasks;
using DSharpPlus.Entities;

namespace PiltoverBot.Services
{
    public static class EmojiHelper
    {
        public static async Task<DiscordEmoji> GetEmojiAsync(DiscordGuild guild, string name)
        {
            if (string.IsNullOrEmpty(name))
                return null;

            if (name.First() != ':')
            {
                name = ':' + name;
            }

            if (name.Last() != ':')
            {
                name = name + ':';
            }
            
            var emojis = await guild.GetEmojisAsync();

            return emojis.FirstOrDefault(x => x.Name == name);
        }

        public static async Task<DiscordEmoji> GetEmojiAsync(DiscordGuild guild, ulong id)
        {
            return await guild.GetEmojiAsync(id);
        }
    }
}