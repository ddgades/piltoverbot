﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;

namespace PiltoverBot.Services
{
    public class Lobby
    {
        public ulong HostId { get; set; }

        public ulong MessageChannel { get; set; }

        public ulong MessageId { get; set; }

        public ulong LobbyChannelId { get; set; }

        public List<ulong> LobbyRoles { get; set; }
    }


    public class LobbyManager : IDisposable
    {
        private readonly Dictionary<ulong, Lobby> _lobbies;
        private readonly DiscordClient _dsClient;
        public LobbyManager(DiscordClient dsClient)
        {
            _dsClient = dsClient;
            _lobbies = new Dictionary<ulong, Lobby>();

            dsClient.VoiceStateUpdated += DsClient_VoiceStateUpdated;
        }

        private async Task DsClient_VoiceStateUpdated(DiscordClient sender, VoiceStateUpdateEventArgs e)
        {
            var lobby = _lobbies.FirstOrDefault(x => x.Value.LobbyChannelId == e.Before?.Channel.Id);

            if (lobby.Value == null)
            {
                return;
            }

            var member = await e.Guild.GetMemberAsync(e.User.Id);

            if ((e.After.Channel == null || !e.After.Channel.Users.Contains(member)) &&
                lobby.Value.HostId == e.User.Id)
            {
                _lobbies.Remove(lobby.Key);
                await e.Guild.Channels[lobby.Value.LobbyChannelId].DeleteAsync();

                var message = await e.Guild.Channels[lobby.Value.MessageChannel].GetMessageAsync(lobby.Value.MessageId);
                await e.Guild.Channels[lobby.Value.MessageChannel].DeleteMessageAsync(message);
                return;
            }

            if (e.After.Channel == null || !e.After.Channel.Users.Contains(member))
            {
                var message = await e.Guild.Channels[lobby.Value.MessageChannel].GetMessageAsync(lobby.Value.MessageId);

                await message.ModifyAsync(builder => RemoveMemberMention(builder, e.User.Mention));
                return;
            }

            if (e.After.Channel is not null && (e.Before.Channel == null || !e.Before.Channel.Users.Contains(member)) && e.After.Channel.Users.Contains(member))
            {
                var message = await e.Guild.Channels[lobby.Value.MessageChannel].GetMessageAsync(lobby.Value.MessageId);
                await message.ModifyAsync(builder => AddMemberMention(builder, e.User.Mention));
                return;
            }
        }

        public void AddLobby(Lobby lobby)
        {
            _lobbies.Add(lobby.MessageId, lobby);
        }


        private void RemoveMemberMention(DiscordMessageBuilder builder, string member)
        {
            var eBuilder = new DiscordEmbedBuilder();
            foreach (var embed in builder.Embed.Fields)
            {
                var match = Regex.Match(embed.Value, @"(@.+):");
                if (embed.Name == Constants.Embeds.Member && match.Groups[0].Value == member)
                {
                    continue;
                }

                eBuilder.AddField(embed.Name, embed.Value);
            }

            builder.WithEmbed(eBuilder.Build());
        }

        private void AddMemberMention(DiscordMessageBuilder builder, string member)
        {
            var eBuilder = new DiscordEmbedBuilder(builder.Embed);
            //TODO add summoner names
            eBuilder.AddField(Constants.Embeds.Member, string.Format(Constants.Embeds.MemberMentionFormat, member, ""));
            
            builder.WithEmbed(eBuilder.Build());
        }

        public void Dispose()
        {
            _dsClient.VoiceStateUpdated -= DsClient_VoiceStateUpdated;
        }
    }

}
