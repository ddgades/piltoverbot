﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Data.Entity;
using Data.Mongo.Repository.Abstracts;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using PiltoverBot.Services;

namespace PiltoverBot.Commands
{
    public class AuthenticationCommands : BaseCommandModule
    {
        private readonly RiotService _riotService;

        private readonly MongoRepository _repository;
        public AuthenticationCommands(RiotService riotService, MongoRepository repository)
        {
            _riotService = riotService;
            _repository = repository;
        }

        [Command("auth")]
        public async Task AuthCommand(CommandContext ctx, string summonerName)
        {
            var summoner = await _riotService.GetSummonerAsync(summonerName);
            if (summoner == null)
            {
                await ctx.Member.SendMessageAsync($"Ops!");
                return;
            }

            await ctx.Message.DeleteAsync();
            await ctx.Member.SendMessageAsync($"Greetings {summonerName}! Please follow next few steps for bind adjust your profile with summoner information");

            var member = await _repository.GetOneAsync<Data.Mongo.Entity.Member>(x => x.DiscordMemberId == ctx.Member.Id);

            var existedSummonerAccount = member.SummonerAccounts?.FirstOrDefault(x => x.Name == summonerName);
            if (existedSummonerAccount != null)
            {
                //here case when summoner adjusted to some member already
                member.SummonerAccounts.Remove(existedSummonerAccount);
            }

            var authGuid = Guid.NewGuid();

            var summonerAccountEntity = new SummonerAccount()
            {
                SummonerId = summoner.Id,
                Puuid = summoner.Puuid,
                IsConfirmed = false,
                Name = summonerName,
                ConfirmationCode = authGuid.ToString(),
            };
            await _repository.UpdateOneAsync(member);
            await ctx.Member.SendMessageAsync($"Please put this code into confirmation section in your client {authGuid}");
        }

        [Command("confirm")]
        public async Task ConfirmCommand(CommandContext ctx, string authGuid)
        {
            var member = await _repository.GetOneAsync<Data.Mongo.Entity.Member>(x => x.DiscordMemberId == ctx.Member.Id
                && x.SummonerAccounts.Any(s => s.ConfirmationCode == authGuid && !s.IsConfirmed));

            if (member == null)
            {
                await ctx.Member.SendMessageAsync($"Ops!");
                return;
            }

            var summonerAccount = member.SummonerAccounts.First(x => x.ConfirmationCode == authGuid);

            await ctx.Message.DeleteAsync();
            var codeFromRiot = await _riotService.GetThirdPartyCode(summonerAccount.SummonerId);

            if (codeFromRiot == summonerAccount.ConfirmationCode)
            {
                summonerAccount.IsConfirmed = true;
                
                await _repository.UpdateOneAsync(member);
                await ctx.Member.SendMessageAsync($"Cool now you authorized as {summonerAccount.Name}");
            }
            else
            {
                await ctx.Member.SendMessageAsync($"Codes not match");
            }
        }

        [Command("me")]
        public async Task MeCommand(CommandContext ctx)
        {
            var member = await _repository.GetOneAsync<Data.Mongo.Entity.Member>(x => x.DiscordMemberId == ctx.Member.Id);

            foreach (var memberSummonerAccount in member.SummonerAccounts)
            {
                await ctx.RespondAsync(builder =>
                {
                    builder.WithContent($"{memberSummonerAccount.Name}");
                });
            }
        }
    }
}
