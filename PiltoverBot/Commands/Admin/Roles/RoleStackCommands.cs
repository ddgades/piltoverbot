﻿using System.Text;
using System.Threading.Tasks;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;

namespace PiltoverBot.Commands.Admin.Roles
{
    
    public class RoleStackCommands:BaseCommandModule
    {
        /*
        private readonly IMemberRepository<RoleStack> _repository;
        private readonly IMemberRepository<VirtualGuild> _virtualGuildRepository;
        public RoleStackCommands(IMemberRepository<RoleStack> repository, IMemberRepository<VirtualGuild> virtualGuildRepository)
        {
            _repository = repository;
            _virtualGuildRepository = virtualGuildRepository;
        }

        [RequireRoles(RoleCheckMode.Any,new [] { Constants.Roles.Sheriff , Constants.Roles.Redactor} )]
        [Command("add-role-stack")]
        public async Task CreateRoleStack(CommandContext ctx, string roleStack)
        {
            var guild = await _virtualGuildRepository.AsQueryable()
                .FilterByGuild(ctx.Guild.Id)
                .FirstOrDefaultAsync();

            var publish = await _repository.AsQueryable()
                .FilterByGuild(guild.Id)
                .FilterByName(roleStack)
                .FirstOrDefaultAsync();

            if (publish != null)
            {
                await ctx.RespondAsync($"Already exists");
                return;
            }

            var entityToCreate = new RoleStack()
            {
                Name = roleStack,
                GuildId = guild.Id,
            };

            await _repository.AddAsync(entityToCreate);

            await ctx.RespondAsync($"Done");
        }
        [RequireRoles(RoleCheckMode.Any,new [] { Constants.Roles.Sheriff , Constants.Roles.Redactor} )]
        [Command("delete-role-stack")]
        public async Task DeleteRoleStack(CommandContext ctx, string roleStack)
        {
            var guild = await _virtualGuildRepository.AsQueryable()
                .FilterByGuild(ctx.Guild.Id)
                .FirstOrDefaultAsync();

            var publish = await _repository.AsQueryable()
                .FilterByGuild(guild.Id)
                .FilterByName(roleStack)
                .FirstOrDefaultAsync();

            if (publish == null)
            {
                await ctx.RespondAsync($"Not Found");
                return;
            }

            await _repository.RemoveAsync(publish);

            await ctx.RespondAsync($"Done");
        }


        [Command("role-stacks")]
        public async Task ListRoleStacks(CommandContext ctx)
        {
            var guild = await _virtualGuildRepository.AsQueryable()
                .FilterByGuild(ctx.Guild.Id)
                .FirstOrDefaultAsync();

            var roleStacks = await _repository.AsQueryable()
                .FilterByGuild(guild.Id)
                .ToListAsync();

            if (!roleStacks.Any())
            {
                await ctx.RespondAsync($"Not Found");
                return;
            }

            var builder = new StringBuilder();
            foreach (var entity in roleStacks)
            {
                builder.AppendLine($"{entity.Name}-{entity.Id}");
            }
            await ctx.RespondAsync(builder.ToString());
        }
        */
    }
        
}
