﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Mongo.Entity;
using Data.Mongo.Repository.Abstracts;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Interactivity.Extensions;

namespace PiltoverBot.Commands.Admin.Roles
{
    public class RolesCommands : BaseCommandModule
    {
        private readonly MongoRepository _repository;
        /*
        [Command("roles")]
        public async Task CreatePublishType(CommandContext ctx)
        {
            var guild = await _repository.CountAsync<Guild>(x => x.GuildId == ctx.Guild.Id);

            if (stacks == null || !stacks.Any() || bindings == null || !bindings.Any())
            {
                await ctx.RespondAsync($"Not Found");
                return;
            }
            var interactivity = ctx.Client.GetInteractivity();

            foreach (var stack in stacks)
            {
                var stackBindings = bindings.Where(x => x.StackId == stack.Id);

                var member = await _memberRepository.AsQueryable().FilterById(ctx.Member.Id).FirstOrDefaultAsync();

                var message = await ctx.RespondAsync($"For stack {stack.Name} your roles are {string.Join("|", member.Roles?.Select(x => x.Name) ?? new[] { string.Empty })}");

                var reaction = await interactivity.WaitForReactionAsync(args =>
                        args.Message.Id == message.Id && stackBindings.Any(x => x.EmojiId == args.Emoji.Id), TimeSpan.FromHours(1));

                if (!reaction.TimedOut)
                {
                    var binding = bindings.FirstOrDefault(x => x.EmojiId == reaction.Result.Emoji.Id);
                    if (binding == null)
                    {
                        return;
                    }

                    var entity = new VirtualRole()
                    {
                        RoleBindingId = binding.Id,
                        MemberId = ctx.Member.Id,
                        Name = $"{ctx.Member.Id}_{binding.Id}"
                    };

                    member.Roles ??= new List<VirtualRole>();
                    var toRemove = member.Roles.Where(x => x.RoleBinding.StackId == binding.StackId).ToList();

                    if (toRemove.Any())
                    {
                        foreach (var roleToRemove in toRemove)
                        {
                            member.Roles.Remove(roleToRemove);
                            await ctx.Member.RevokeRoleAsync(ctx.Guild.Roles[roleToRemove.RoleBinding.RoleId]);
                        }
                    }

                    member.Roles.Add(entity);

                    await _memberRepository.UpdateAsync(member);

                    await ctx.Member.GrantRoleAsync(ctx.Guild.Roles[binding.RoleId]);
                }
            }

            await ctx.RespondAsync($"Done");
        }
        */
    }
}
