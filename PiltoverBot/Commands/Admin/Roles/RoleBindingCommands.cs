﻿using System.Text;
using System.Threading.Tasks;
using Data.Entity;
using Data.Mongo.Entity;
using Data.Mongo.Repository.Abstracts;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

namespace PiltoverBot.Commands.Admin.Roles
{
    public class RoleBindingCommands : BaseCommandModule
    {
        private readonly MongoRepository _repository;

        public RoleBindingCommands(MongoRepository repository)
        {
            _repository = repository;
        }

        /*
        [Command("add-role-binding")]
        public async Task CreateRoleBinding(CommandContext ctx, ulong roleId, DiscordEmoji emoji, ulong stackId)
        {
            var guild = await _repository.GetOneAsync<Guild>(x => x.GuildId == ctx.Guild.Id);

            if (stack == null || !ctx.Guild.Roles.ContainsKey(roleId) || !ctx.Guild.Emojis.ContainsKey(emoji.Id))
            {
                await ctx.RespondAsync($"Not found");
                return;
            }

            var name = $"{stack.Name}_{roleId}_{emoji.Id}";

            var binding = await _bindingRepository.AsQueryable()
                .FilterByGuild(guild.Id)
                .FilterByName(name)
                .FirstOrDefaultAsync();

            if (binding != null)
            {
                await ctx.RespondAsync($"Already exists");
                return;
            }

            var entityToCreate = new RoleBinding()
            {
                Name = name,
                StackId = stackId,
                RoleId = roleId,
                EmojiId = emoji.Id,
                GuildId = guild.Id,
            };

            await _bindingRepository.AddAsync(entityToCreate);
            await ctx.RespondAsync($"Done");
        }

        [Command("delete-role-binding")]
        public async Task DeleteRoleBinding(CommandContext ctx, string bindingName)
        {
            var guild = await _virtualGuildRepository.AsQueryable()
                .FilterByGuild(ctx.Guild.Id)
                .FirstOrDefaultAsync();

            var binding = await _bindingRepository.AsQueryable()
                .FilterByGuild(guild.Id)
                .FilterByName(bindingName)
                .FirstOrDefaultAsync();

            if (binding == null)
            {
                await ctx.RespondAsync($"Not Found");
                return;
            }

            await _bindingRepository.RemoveAsync(binding);

            await ctx.RespondAsync($"Done");
        }


        [Command("role-bindings")]
        public async Task ListRoleBinding(CommandContext ctx)
        {
            var guild = await _virtualGuildRepository.AsQueryable()
                .FilterByGuild(ctx.Guild.Id)
                .FirstOrDefaultAsync();

            var bindings = await _bindingRepository.AsQueryable()
                .FilterByGuild(guild.Id)
                .ToListAsync();

            if (!bindings.Any())
            {
                await ctx.RespondAsync($"Not Found");
                return;
            }

            var builder = new StringBuilder();
            foreach (var binding in bindings)
            {
                builder.AppendLine($"{binding.Name}-{binding.StackId}");
            }
            await ctx.RespondAsync(builder.ToString());
        }

        private DiscordEmoji getEmoji(CommandContext ctx, string emojiName)
        {
            return DiscordEmoji.FromName(ctx.Client, emojiName);
        }
        */
    }
}
