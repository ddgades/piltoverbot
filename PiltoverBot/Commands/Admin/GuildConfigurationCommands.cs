﻿using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Data.Mongo.Entity;
using Data.Mongo.Repository.Abstracts;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

namespace PiltoverBot.Commands.Admin
{

    public class GuildConfigurationCommands : BaseCommandModule
    {
        private readonly MongoRepository _repository;
        public GuildConfigurationCommands(MongoRepository repository)
        {
            _repository = repository;
        }

        //[RequireUserPermissions(Permissions.Administrator)]
        [Command("download-config")]
        public async Task DownloadConfig(CommandContext ctx)
        {
            var guild = await _repository.GetOneAsync<Guild>(x => x.GuildId == ctx.Guild.Id);
            var dmChannel = await ctx.Member.CreateDmChannelAsync();
            string json;
            if (guild.GuildConfiguration == null)
            {
                json = await File.ReadAllTextAsync(Path.GetFullPath(Constants.ConfigurationPath));
            }
            else
            {
                json = Newtonsoft.Json.JsonConvert.SerializeObject(guild.GuildConfiguration);
            }

            
            await using var mem = new MemoryStream(Encoding.ASCII.GetBytes(json));
            var message = await dmChannel
                .SendMessageAsync(builder => builder.WithFile("Config.json", mem));

            await ctx.RespondAsync($"Done");
        }


        //[RequireUserPermissions(Permissions.Administrator)]
        [Command("upload-config")]
        public async Task UploadConfig(CommandContext ctx)
        {
            var guild = await _repository.GetOneAsync<Guild>(x => x.GuildId == ctx.Guild.Id);
            var dmChannel = await ctx.Member.CreateDmChannelAsync();
            var file = ctx.Message.Attachments.FirstOrDefault(x => Path.GetExtension(x.FileName) == ".json");
            if (file == null)
            {
                await dmChannel
                    .SendMessageAsync("Ops");
                return;
            }

            using (var httpClient = new HttpClient())
            {
                var result = await httpClient.GetAsync(file.Url);
                if (!result.IsSuccessStatusCode)
                {
                    await dmChannel
                        .SendMessageAsync("Ops");
                    return;
                }

                var st = await result.Content.ReadAsStringAsync();
                var configuration = Newtonsoft.Json.JsonConvert.DeserializeObject<GuildConfiguration>(st);

                if (configuration?.RoleBindings != null)
                    foreach (var roleBinding in configuration.RoleBindings)
                    {
                        roleBinding.EmojiId ??= DiscordEmoji.FromName(ctx.Client, roleBinding.EmojiName).Id;
                    }

                guild.GuildConfiguration = configuration;
            }

            await _repository.UpdateOneAsync(guild);
            await ctx.RespondAsync($"Done");
        }
    }
}
