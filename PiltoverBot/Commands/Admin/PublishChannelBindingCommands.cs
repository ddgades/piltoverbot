﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Entity;
using Data.Mongo.Entity;
using Data.Mongo.Repository.Abstracts;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;

namespace PiltoverBot.Commands.Admin
{
    [RequireRoles(RoleCheckMode.Any, new[] { Constants.Roles.Sheriff, Constants.Roles.Redactor })]
    public class PublishChannelBindingCommands : BaseCommandModule
    {
        private readonly MongoRepository _repository;
        public PublishChannelBindingCommands(MongoRepository repository)
        {
            _repository = repository;
        }
        /*
        [Command("add-channel-binding")]
        public async Task CreatePublishType(CommandContext ctx, ulong channelForApproveId, ulong channelForPublishId, string publishType)
        {
            var guild = await _repository.GetOneAsync<Guild>(x => x.GuildId == ctx.Guild.Id);

            
            //  x.Value.ApprovalChannelId != channelForApproveId && x.Value.PublishChannelId != channelForPublishId);

            if (guild.PublishChannelBindings != null && 
                (guild.PublishChannelBindings.ContainsKey(publishType)
                 || !ctx.Guild.Channels.ContainsKey(channelForApproveId)
                 || !ctx.Guild.Channels.ContainsKey(channelForPublishId)))
            {
                await ctx.RespondAsync("ops");
                return;
            }

            guild.PublishChannelBindings ??= new Dictionary<string, PublishChannelBinding>();

            guild.PublishChannelBindings.Add(publishType, new PublishChannelBinding()
            {
                Name = publishType,
                ApprovalChannelId = channelForApproveId,
                PublishChannelId = channelForPublishId,
            });

            await _repository.UpdateOneAsync(guild);
            await ctx.RespondAsync($"Done");
        }

        [Command("delete-channel-binding")]
        public async Task DeletePublishType(CommandContext ctx, string bindingName)
        {
            var guild = await _repository.GetOneAsync<Guild>(x => x.GuildId == ctx.Guild.Id);

            guild.PublishChannelBindings.Remove(bindingName);

            await _repository.UpdateOneAsync(guild);

            await ctx.RespondAsync($"Done");
        }


        [Command("channel-bindings")]
        public async Task ListPublishTypes(CommandContext ctx)
        {
            var guild = await _repository.GetOneAsync<Guild>(x => x.GuildId == ctx.Guild.Id);

            if (!guild.PublishChannelBindings.Any())
            {
                await ctx.RespondAsync($"Not Found");
                return;
            }

            var builder = new StringBuilder();

            foreach (var binding in guild.PublishChannelBindings)
            {
                builder.AppendLine($"{binding.Key}");
            }

            await ctx.RespondAsync(builder.ToString());
        }
        */
    }
}
