﻿using System.Linq;
using System.Threading.Tasks;
using Data.Entity;
using Data.Mongo.Entity;
using Data.Mongo.Repository.Abstracts;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using PiltoverBot.Properties;

namespace PiltoverBot.Commands.Admin
{
    [RequireRoles(RoleCheckMode.Any, Constants.Roles.Sheriff, Constants.Roles.Redactor)]
    public class AdminCommands : BaseCommandModule
    {
        private readonly MongoRepository _repository;

        public AdminCommands(MongoRepository repository)
        {
            _repository = repository;
        }

        [Command("load-members")]
        public async Task LoadMembers1(CommandContext ctx)
        {
            var guildCount = await _repository.CountAsync<Guild>(x => x.GuildId == ctx.Guild.Id);

            if (guildCount == 0)
                await _repository.AddOneAsync(new Guild
                {
                    GuildId = ctx.Guild.Id,
                    Name = ctx.Guild.Name
                });

            var guild = await _repository.GetOneAsync<Guild>(x => x.GuildId == ctx.Guild.Id);
            var mem = await ctx.Guild.GetAllMembersAsync();

            var existed = await _repository.GetManyAsync<Member>(x => x.GuildId == guild.Id);
            foreach (var memberSummonerAccount in mem.Where(x => !x.IsBot))
            {
                var ext = existed.FirstOrDefault(x => x.DiscordMemberId == memberSummonerAccount.Id);
                if (ext != null)
                {
                    ext.Name = memberSummonerAccount.DisplayName;

                    await _repository.UpdateOneAsync(ext);
                    continue;
                }

                await _repository.AddOneAsync(new Member
                {
                    DiscordMemberId = memberSummonerAccount.Id,
                    Name = memberSummonerAccount.DisplayName,
                    GuildId = guild.Id
                });
            }

            var membersInBase = await _repository.CountAsync<Member>(x => x.GuildId == guild.Id);
            await ctx.RespondAsync(string.Format(Resources.load_members_success, membersInBase));
        }


        [Command("configure-lobby")]
        public async Task LoadMembers(CommandContext ctx, ulong defaultChannelId)
        {
            var guild = await _repository.GetOneAsync<Guild>(x => x.GuildId == ctx.Guild.Id);

            if (guild.GuildConfiguration.BulletinBoardConfiguration != null)
            {
                var channelsToDelete = new[]
                {
                    guild.GuildConfiguration.BulletinBoardConfiguration.CategoryId, guild.GuildConfiguration.BulletinBoardConfiguration.BulletinBoardChannelId
                };
                foreach (var channel in channelsToDelete)
                    if (ctx.Guild.Channels.ContainsKey(channel))
                        await ctx.Guild.Channels[channel].DeleteAsync();

                guild.GuildConfiguration.BulletinBoardConfiguration = null;
                await _repository.UpdateOneAsync(guild);
            }

            var bot = ctx.Guild.Members.FirstOrDefault(x => x.Value.IsBot && x.Value.DisplayName == Constants.BotName);

            var builder = new DiscordOverwriteBuilder();
            builder.Allow(Permissions.All);

            foreach (var discordRole in ctx.Guild.Roles.Where(x => x.Value.Name == Constants.Roles.Officer ||
                                                                   x.Value.Name == Constants.Roles.Sheriff)
                .Select(x => x.Value))
                builder.For(discordRole);

            var category = await ctx.Guild.CreateChannelCategoryAsync("PB Private Lobbies", new[] {builder});


            category.PermissionsFor(bot.Value);
            DiscordRole hostRole;
            if (ctx.Guild.Roles.Values.All(x => x.Name != "Lobby Host"))
                hostRole = await ctx.Guild.CreateRoleAsync("Lobby Host");
            else
                hostRole = ctx.Guild.Roles.Values.FirstOrDefault(x => x.Name == "Lobby Host");

            builder = new DiscordOverwriteBuilder();
            builder.Deny(Permissions.AddReactions);
            builder.Deny(Permissions.SendMessages);
            builder.For(ctx.Guild.EveryoneRole);
            builder.For(hostRole);

            var bbChannel = await ctx.Guild.CreateTextChannelAsync("PB Bulletin board", overwrites: new[] {builder});

            guild.GuildConfiguration.BulletinBoardConfiguration = new BulletinBoardConfiguration
            {
                BulletinBoardChannelId = bbChannel.Id,
                CategoryId = category.Id,
                HostRoleId = hostRole.Id,
                DefaultChannelId = defaultChannelId
            };

            await _repository.UpdateOneAsync(guild);

            await ctx.RespondAsync("Done");
        }
    }
}