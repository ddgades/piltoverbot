﻿using Data.Mongo.Repository.Abstracts;
using DSharpPlus.CommandsNext;

namespace PiltoverBot.Commands.Admin
{

    public class PublishTypesCommands : BaseCommandModule
    {
        private readonly MongoRepository _repository;
        public PublishTypesCommands(MongoRepository repository)
        {
            _repository = repository;
        }

        /*
        [RequireRoles(RoleCheckMode.Any, new[] { Constants.Roles.Sheriff, Constants.Roles.Redactor })]
        [Command("add-publish-type")]
        public async Task CreatePublishType(CommandContext ctx, string publishType)
        {
            var guild = await _repository.GetOneAsync<Guild>(x => x.GuildId == ctx.Guild.Id);

            if (guild.PublishTypes.Any(x => x.Name == publishType))
            {
                await ctx.RespondAsync($"Already exists");
                return;
            }

            guild.PublishTypes.Add(new Data.Entity.PublishType()
            {
                Name = publishType,
            });

            await _repository.UpdateOneAsync(guild);

            await ctx.RespondAsync($"Done");
        }
        [RequireRoles(RoleCheckMode.Any, new[] { Constants.Roles.Sheriff, Constants.Roles.Redactor })]
        [Command("delete-publish-type")]
        public async Task DeletePublishType(CommandContext ctx, string publishType)
        {
            var guild = await _repository.GetOneAsync<Guild>(x => x.GuildId == ctx.Guild.Id);

            if (guild.PublishTypes.All(x => x.Name != publishType))
            {
                await ctx.RespondAsync($"Not Found");
                return;
            }

            guild.PublishTypes.RemoveAll(x => x.Name == publishType);

            await _repository.UpdateOneAsync(guild);

            await ctx.RespondAsync($"Done");
        }


        [Command("publish-types")]
        public async Task ListPublishTypes(CommandContext ctx)
        {
            var guild = await _repository.GetOneAsync<Guild>(x => x.GuildId == ctx.Guild.Id);

            if (guild.PublishTypes == null)
            {
                await ctx.RespondAsync($"Not Found");
                return;
            }

            var builder = new StringBuilder();
            foreach (var publish in guild.PublishTypes)
            {
                builder.AppendLine($"{publish.Name}");
            }
            await ctx.RespondAsync(builder.ToString());
        }
        */
    }
}
