﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Mongo.Entity;
using Data.Mongo.Repository.Abstracts;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using PiltoverBot.Services;

namespace PiltoverBot.Commands
{
    public class LobbyCommands : BaseCommandModule
    {
        private readonly LobbyManager _lobbyManager;
        private readonly MongoRepository _repository;
        public LobbyCommands(LobbyManager lobbyManager, MongoRepository repository)
        {
            _lobbyManager = lobbyManager;
            _repository = repository;
        }


        [Command("lobby")]
        public async Task LoadMembers(CommandContext ctx)
        {
            var guild = await _repository.GetOneAsync<Guild>(x => x.GuildId == ctx.Guild.Id);

            if (ctx.Member.VoiceState == null)
            {
                return;
            }

          
            var voice = await ctx.Guild.CreateVoiceChannelAsync($"PB {ctx.Member.DisplayName}_VoiceLobby",
            ctx.Guild.Channels[guild.GuildConfiguration.BulletinBoardConfiguration.CategoryId]);

            var message = await ctx.Guild.Channels[guild.GuildConfiguration.BulletinBoardConfiguration.BulletinBoardChannelId]
                .SendMessageAsync(builder => BuildMessage(builder, ctx.User, voice));

            var lobby = new Lobby
            {
                HostId = ctx.Member.Id,
                LobbyRoles = new List<ulong>(),
                LobbyChannelId = voice.Id,
                MessageId = message.Id,
                MessageChannel = message.ChannelId
            };

            await ctx.Member.PlaceInAsync(voice);

            _lobbyManager.AddLobby(lobby);
            await ctx.RespondAsync($"Done");
        }

        private DiscordMessageBuilder BuildMessage(DiscordMessageBuilder builder, DiscordUser author, DiscordChannel channel)
        {
            var eBilder = new DiscordEmbedBuilder();
            eBilder.AddField(Constants.Embeds.Author, author.Mention);
            eBilder.AddField(Constants.Embeds.Lobby, channel.Mention);
            builder.WithEmbed(eBilder);

            return builder;
        }
    }
}
