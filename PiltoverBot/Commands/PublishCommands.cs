﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Data.Mongo.Entity;
using Data.Mongo.Repository.Abstracts;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.Extensions;

namespace PiltoverBot.Commands
{
    public class PublishCommands : BaseCommandModule
    {
        private readonly MongoRepository _repository;
        public PublishCommands(MongoRepository repository)
        {
            _repository = repository;
        }


        [Command("publish")]
        public async Task PublishCommand(CommandContext ctx, string publishType)
        {
            var guild = await _repository.GetOneAsync<Guild>(x => x.GuildId == ctx.Guild.Id);
            var binding = guild.GuildConfiguration.PublishChannelBindings.FirstOrDefault(x => x.Name == publishType);
            if (binding==null)
            {
                await ctx.RespondAsync($"Not Found");
                return;
            }

            

            await ctx.RespondAsync("sand message here");
            var content = await ctx.Message.GetNextMessageAsync(TimeSpan.FromMinutes(2));

            var interactivity = ctx.Client.GetInteractivity();
            
            var approveMessage = await ctx.Guild.Channels[binding.ApprovalChannelId].SendMessageAsync(builder => BuildMessage(builder, content.Result, ctx.User));

            var reaction = await interactivity.WaitForReactionAsync(args =>
                    args.Message.Id == approveMessage.Id && args.Emoji.Name == Constants.DiscordEmoji.Approved
            , TimeSpan.FromDays(1));

            if (!reaction.TimedOut)
            {
                await ctx.Guild.Channels[binding.PublishChannelId].SendMessageAsync(builder => BuildMessage(builder, content.Result, ctx.User));
            }
        }

        private DiscordMessageBuilder BuildMessage(DiscordMessageBuilder builder, DiscordMessage message, DiscordUser author)
        {
            var eBilder = new DiscordEmbedBuilder();

            eBilder.AddField("Author", author.Mention);
            foreach (var attachment in message.Attachments)
            {
                eBilder.WithImageUrl(attachment.Url);
            }
            builder.WithContent(message.Content)
                .WithEmbed(eBilder);

            return builder;
        }
    }
}
