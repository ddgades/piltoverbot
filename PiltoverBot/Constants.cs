﻿namespace PiltoverBot
{
    public static class Constants
    {
        public const string ApiKey = "RGAPI-052655ee-765e-49cf-982e-aa30ede81436";

        public const string DiscordToken = "ODU0MDQ3NzM3MzUyMDI4MTgx.YMeP8A.6876M1qCIBre9DY4r-erMYgJgRM";

        public const string BotName = "PiltoverBot";

        public const string ConfigurationPath = @"Configuration\Files\configuration-example.json";

        public static class DiscordEmoji
        {
            public const string Top = "role_TOP";
            public const string Jungle = "role_JUNGLE";
            public const string Mid = "role_MID";
            public const string Bottom = "role_ADC";
            public const string Support = "role_SUPP";

            public const string PiltoverCrest = "Piltover_Crest";
            public const string ZaunCrest = "Zaun_Crest";

            public const string Approved = "approved";
            public const string Rejected = "rejected";


        }

        public static class Roles
        {
            public const string Redactor = "Redactor";
            public const string Sheriff = "Sheriff";
            public const string Senior = "Senior";
            public const string Officer = "Officer";
            public const string Summoner = "Summoner";
            public const string Citizen = "Citizen";


            public const string Piltover = "Piltover";
            public const string Zaun = "Zaun";

        }

        public static class Embeds
        {
            public const string Author = "Author";
            public const string Member = "Member";
            public const string Lobby = "Lobby";

            public const string MemberMentionFormat = "{0}: {1}";
        }
    }
}