﻿using System;
using System.Collections.Generic;
using Data.Entity;
using Data.Mongo.Data;

namespace Data.Mongo.Entity
{
    public class Guild: GuildEntity, IDocument<Guid>
    {
        public GuildConfiguration  GuildConfiguration{ get; set; }

        //public Dictionary<string, PublishChannelBinding> PublishChannelBindings { get; set; }

        //public Dictionary<string, RoleBinding> RoleBindings { get; set; }

    }

    public class Member: MemberEntity,IDocument<Guid>
    { 
        public virtual List<RoleEntity> Roles { get; set; }
        public virtual List<SummonerAccount> SummonerAccounts { get; set; }
    }

    public class GuildConfiguration
    {
        public BulletinBoardConfiguration BulletinBoardConfiguration { get; set; }

        public PublishChannelBinding[] PublishChannelBindings { get; set; }

        public RoleBinding[] RoleBindings { get; set; }
    }

    public class RoleBinding
    {
        public string Name { get; set; }

        public string Stack { get; set; }

        public ulong RoleId { get; set; }

        public string EmojiName { get; set; }

        public ulong? EmojiId { get; set; }
    }

    public class PublishChannelBinding
    {
        public string Name { get; set; }
        
        public ulong ApprovalChannelId { get; set; }

        public ulong PublishChannelId { get; set; }
    }

    public class GameEvent
    {
        public string Name { get; set; }
        
        public string Period { get; set; }

        public string Type { get; set; }
    }
}