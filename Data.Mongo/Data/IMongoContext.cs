﻿using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Data.Mongo.Data
{
    public interface IMongoContext
    {
        IMongoClient MongoClient { get; }

        IMongoDatabase MongoDatabase { get; }

        IMongoCollection<TDocument> GetCollection<TDocument>();
    }

    public class MongoContext : IMongoContext
    {
        private readonly Func<Type, string> _connectionNameProvider;

        public MongoContext(IOptions<MongoContextConfig> options)
        {
            string connectionString = options.Value.ConnectionString;
            this.MongoClient = new MongoDB.Driver.MongoClient(connectionString);
            this.MongoDatabase = this.MongoClient.GetDatabase(new MongoUrl(connectionString).DatabaseName);
            this._connectionNameProvider = options.Value.CollectionNameProvider ?? (documentType => documentType.Name ?? "");
        }

        public MongoContext(MongoContextConfig cfg)
        {
            string connectionString = cfg.ConnectionString;
            this.MongoClient = new MongoDB.Driver.MongoClient(connectionString);
            this.MongoDatabase = this.MongoClient.GetDatabase(new MongoUrl(connectionString).DatabaseName);
            this._connectionNameProvider = cfg.CollectionNameProvider ?? (documentType => documentType.Name ?? "");
        }

        public IMongoClient MongoClient { get; }

        public IMongoDatabase MongoDatabase { get; }

        public IMongoCollection<TDocument> GetCollection<TDocument>() => this.MongoDatabase.GetCollection<TDocument>(this.GetCollectionName<TDocument>());

        protected virtual string GetCollectionName<TDocument>() => this._connectionNameProvider(typeof(TDocument));
    }

    [ExcludeFromCodeCoverage]
    public class MongoContextConfig
    {
        public string ConnectionString { get; set; }

        public Func<Type, string> CollectionNameProvider { get; set; }
    }
}