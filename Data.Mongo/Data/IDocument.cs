﻿using System;

namespace Data.Mongo.Data
{
    public interface IDocument : IDocument<Guid>
    {
    }

    public interface IDocument<TKey> where TKey : IEquatable<TKey>
    {
        TKey Id { get; set; }
    }


}
