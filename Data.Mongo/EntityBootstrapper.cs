﻿using Data.Mongo.Entity;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson.Serialization;

namespace Data.Mongo
{
    public static class EntityBootstrapper
    {
        public static void BootstrapEntities(this IServiceCollection collection)
        {
            BsonClassMap.RegisterClassMap<Guild>(map =>
            {
                map.AutoMap();
                map.SetIgnoreExtraElements(true);
            });

            BsonClassMap.RegisterClassMap<Member>(map =>
            {
                map.AutoMap();
                map.SetIgnoreExtraElements(true);
            });
        }
    }
}
