﻿using Data.Mongo.Data;
using Data.Mongo.Repository.Abstracts;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;

namespace Data.Mongo
{
    public static class MongoBootstrapper
    {
        public static void AddMongo(this IServiceCollection collection)
        {
            var mConf = new MongoConfig()
            {
                DateTimeOffsetRepresentation = DateTimeOffsetRepresentation.String,
                IgnoreExtraElements = true,
                UseDecimals = true,
            };
            var urls = "mongodb://localhost:27017/PiltoverBot";
            //var urls = "mongodb://PiltoverBot:qwerty@localhost:27017/PiltoverBot?serverSelectionTimeoutMS=5000&connectTimeoutMS=10000&authMechanism=SCRAM-SHA-256";
            var contConf = new MongoContextConfig()
            {
                CollectionNameProvider = documentType => documentType.Name ?? "",
                ConnectionString = urls
            };

            collection.BootstrapEntities();

            var enumPack = new ConventionPack
            {
                new EnumRepresentationConvention(BsonType.Int32),
            };

            ConventionRegistry.Register("EnumInt32Convention", enumPack, t => true);

            collection.AddSingleton<MongoConfig>(mConf);
            collection.AddSingleton<MongoContextConfig>(contConf);
            collection.AddSingleton<IMongoContext, MongoContext>();

            collection.AddSingleton<MongoRepository>();
        }
    }
}
