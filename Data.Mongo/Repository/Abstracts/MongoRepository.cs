﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Data.Mongo.Data;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace Data.Mongo.Repository.Abstracts
{
    public class MongoRepository : MongoRepository<Guid>
    {
        public MongoRepository(IMongoContext context) : base(context)
        {
        }
    }

    public class MongoRepository<TKey> : IMongoRepository<TKey> where TKey : IEquatable<TKey>
    {
        private readonly IMongoContext _context;

        public MongoRepository(IMongoContext context) => this._context = context;


        public virtual Task<T> GetAsync<T>(TKey id, CancellationToken cancellationToken = default(CancellationToken))
            where T : IDocument<TKey>
        {
            var collection = GetCollection<T>()
                .OfType<T>();
            var filterDefinition = Builders<T>
                .Filter
                .Eq(x => x.Id, id);

            return collection
                .Find(filterDefinition)
                .SingleOrDefaultAsync(cancellationToken);
        }

        public virtual Task AddOneAsync<TDocument>(
            TDocument document,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return this.GetCollection<TDocument>().InsertOneAsync(document, null, cancellationToken);
        }

        public virtual Task AddManyAsync<TDocument>(
            IEnumerable<TDocument> documents,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            IMongoCollection<TDocument> collection = this.GetCollection<TDocument>();
            IEnumerable<TDocument> documents1 = documents;
            InsertManyOptions options = new InsertManyOptions();
            options.IsOrdered = false;
            CancellationToken cancellationToken1 = cancellationToken;
            return collection.InsertManyAsync(documents1, options, cancellationToken1);
        }

        public virtual Task AddManyOrderedAsync<TDocument>(
            IEnumerable<TDocument> documents,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            IMongoCollection<TDocument> collection = this.GetCollection<TDocument>();
            IEnumerable<TDocument> documents1 = documents;
            InsertManyOptions options = new InsertManyOptions();
            options.IsOrdered = true;
            CancellationToken cancellationToken1 = cancellationToken;
            return collection.InsertManyAsync(documents1, options, cancellationToken1);
        }

        public virtual IMongoCollection<TDocument> GetCollection<TDocument>() where TDocument : IDocument<TKey> => this._context.GetCollection<TDocument>();

        public virtual Task<TDocument> GetByKeyAsync<TDocument>(
            TKey key,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return this.GetCollection<TDocument>().Find(Builders<TDocument>.Filter.Eq(x => x.Id, key)).SingleOrDefaultAsync(cancellationToken);
        }

        public virtual Task<TDocument> GetOneAsync<TDocument>(
            Expression<Func<TDocument, bool>> filter,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return this.GetCollection<TDocument>().Find(Builders<TDocument>.Filter.Where(filter)).SingleOrDefaultAsync(cancellationToken);
        }

        public virtual Task<TDocument> GetOneAsync<TDocument>(
            FilterDefinition<TDocument> filterDefinition,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return this.GetCollection<TDocument>().Find(filterDefinition).SingleOrDefaultAsync(cancellationToken);
        }

        public virtual IFindFluent<TDocument, TDocument> Find<TDocument>(
            Expression<Func<TDocument, bool>> filter)
            where TDocument : IDocument<TKey>
        {
            return this.GetCollection<TDocument>().Find(Builders<TDocument>.Filter.Where(filter));
        }

        public virtual async Task<bool> AnyAsync<TDocument>(
            Expression<Func<TDocument, bool>> filter,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            IMongoCollection<TDocument> collection = this.GetCollection<TDocument>();
            FilterDefinition<TDocument> filter1 = Builders<TDocument>.Filter.Where(filter);
            CountOptions options = new CountOptions();
            options.Limit = new long?(1L);
            CancellationToken cancellationToken1 = cancellationToken;
            return await collection.CountDocumentsAsync(filter1, options, cancellationToken1) > 0L;
        }

        public virtual async Task<IList<TDocument>> GetManyAsync<TDocument>(
            Expression<Func<TDocument, bool>> filter,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return await this.GetCollection<TDocument>().Find(Builders<TDocument>.Filter.Where(filter)).ToListAsync(cancellationToken);
        }

        public virtual async Task<IList<TDocument>> GetManyAsync<TDocument>(
            FilterDefinition<TDocument> filterDefinition,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return await this.GetCollection<TDocument>().Find(filterDefinition, null).ToListAsync(cancellationToken);
        }

        public virtual async Task<IList<TDocument>> GetManyAsync<TDocument, TField>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, TField>> fieldSelector,
            IEnumerable<TField> values,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return await this.GetCollection<TDocument>().Find(Builders<TDocument>.Filter.And(Builders<TDocument>.Filter.Where(filter), Builders<TDocument>.Filter.In(fieldSelector, values))).ToListAsync(cancellationToken);
        }

        public virtual Task<long> CountAsync<TDocument>(
            Expression<Func<TDocument, bool>> filter,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return this.GetCollection<TDocument>().CountDocumentsAsync(Builders<TDocument>.Filter.Where(filter), cancellationToken: cancellationToken);
        }

        public virtual Task<TProjection> ProjectOneAsync<TDocument, TProjection>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, TProjection>> projection,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            IMongoCollection<TDocument> collection = this.GetCollection<TDocument>();
            FilterDefinition<TDocument> filterDefinition = Builders<TDocument>.Filter.Where(filter);
            ProjectionDefinition<TDocument, TProjection> projection1 = Builders<TDocument>.Projection.Expression(projection);
            FilterDefinition<TDocument> filter1 = filterDefinition;
            return collection.Find(filter1).Project(projection1).SingleOrDefaultAsync(cancellationToken);
        }

        public virtual async Task<IList<TProjection>> ProjectManyAsync<TDocument, TProjection>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, TProjection>> projection,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            IMongoCollection<TDocument> collection = this.GetCollection<TDocument>();
            FilterDefinition<TDocument> filterDefinition = Builders<TDocument>.Filter.Where(filter);
            ProjectionDefinition<TDocument, TProjection> projection1 = Builders<TDocument>.Projection.Expression(projection);
            FilterDefinition<TDocument> filter1 = filterDefinition;
            return await collection.Find(filter1).Project(projection1).ToListAsync(cancellationToken);
        }

        public virtual async Task<IList<TDocument>> GetPageAsync<TDocument>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, object>> sortSelector,
            bool ascending = true,
            int skip = 0,
            int take = 50,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            IMongoCollection<TDocument> collection = this.GetCollection<TDocument>();
            FilterDefinition<TDocument> filterDefinition = Builders<TDocument>.Filter.Where(filter);
            SortDefinition<TDocument> sort = ascending ? Builders<TDocument>.Sort.Ascending(sortSelector) : Builders<TDocument>.Sort.Descending(sortSelector);
            FilterDefinition<TDocument> filter1 = filterDefinition;
            return await collection.Find(filter1).Sort(sort).Skip(new int?(skip)).Limit(new int?(take)).ToListAsync(cancellationToken);
        }

        public virtual async Task<IList<TDocument>> GetPageAsync<TDocument>(
            Expression<Func<TDocument, bool>> filter,
            SortDefinition<TDocument> sortDefinition,
            int skip = 0,
            int take = 50,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return await this.GetCollection<TDocument>().Find(Builders<TDocument>.Filter.Where(filter)).Sort(sortDefinition).Skip(new int?(skip)).Limit(new int?(take)).ToListAsync(cancellationToken);
        }

        public virtual IMongoQueryable<TDocument> GetQuery<TDocument>() where TDocument : IDocument<TKey> => this.GetCollection<TDocument>().AsQueryable();

        public virtual Task<DeleteResult> DeleteOneAsync<TDocument>(
            TKey key,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return this.GetCollection<TDocument>().DeleteOneAsync(Builders<TDocument>.Filter.Eq(x => x.Id, key), cancellationToken);
        }

        public virtual Task<DeleteResult> DeleteOneAsync<TDocument>(
            TDocument document,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return this.DeleteOneAsync<TDocument>(document.Id, cancellationToken);
        }

        public virtual Task<DeleteResult> DeleteOneAsync<TDocument>(
            Expression<Func<TDocument, bool>> filter,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return this.GetCollection<TDocument>().DeleteOneAsync(Builders<TDocument>.Filter.Where(filter), cancellationToken);
        }

        public virtual Task<DeleteResult> DeleteManyAsync<TDocument>(
            Expression<Func<TDocument, bool>> filter,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return this.GetCollection<TDocument>().DeleteManyAsync(Builders<TDocument>.Filter.Where(filter), cancellationToken);
        }

        public virtual Task<DeleteResult> DeleteManyAsync<TDocument>(
            IEnumerable<TDocument> documents,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return this.GetCollection<TDocument>().DeleteManyAsync(Builders<TDocument>.Filter.In(x => x.Id, documents.Select(x => x.Id)), cancellationToken);
        }

        public Task<DeleteResult> DeleteManyAsync<TDocument>(
            IEnumerable<TKey> keys,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return this.GetCollection<TDocument>().DeleteManyAsync(Builders<TDocument>.Filter.In(x => x.Id, keys), cancellationToken);
        }

        public virtual Task<ReplaceOneResult> UpdateOneAsync<TDocument>(
            TDocument document,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            IMongoCollection<TDocument> collection = this.GetCollection<TDocument>();
            FilterDefinition<TDocument> filterDefinition = Builders<TDocument>.Filter.Eq(x => x.Id, document.Id);
            IMongoCollection<TDocument> mongoCollection = collection;
            FilterDefinition<TDocument> filter = filterDefinition;
            TDocument replacement = document;
            UpdateOptions options = new UpdateOptions();
            options.IsUpsert = false;
            CancellationToken cancellationToken1 = cancellationToken;
            return mongoCollection.ReplaceOneAsync(filter, replacement, options, cancellationToken1);
        }

        public virtual Task<UpdateResult> UpdateOneAsync<TDocument>(
            TKey key,
            UpdateDefinition<TDocument> updateDefinition,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return this.GetCollection<TDocument>().UpdateOneAsync(Builders<TDocument>.Filter.Eq(x => x.Id, key), updateDefinition, cancellationToken: cancellationToken);
        }

        public virtual Task<UpdateResult> UpdateOneAsync<TDocument>(
            Expression<Func<TDocument, bool>> filter,
            UpdateDefinition<TDocument> updateDefinition,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return this.GetCollection<TDocument>().UpdateOneAsync(Builders<TDocument>.Filter.Where(filter), updateDefinition, cancellationToken: cancellationToken);
        }

        public virtual Task<UpdateResult> UpdateOneAsync<TDocument, TField>(
            TKey key,
            Expression<Func<TDocument, TField>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return this.GetCollection<TDocument>().UpdateOneAsync(Builders<TDocument>.Filter.Eq(x => x.Id, key), Builders<TDocument>.Update.Set(field, value), cancellationToken: cancellationToken);
        }

        public virtual Task<UpdateResult> UpdateOneAsync<TDocument, TField>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, TField>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            IMongoCollection<TDocument> collection = this.GetCollection<TDocument>();
            FilterDefinition<TDocument> filterDefinition = Builders<TDocument>.Filter.Where(filter);
            UpdateDefinition<TDocument> updateDefinition = Builders<TDocument>.Update.Set(field, value);
            FilterDefinition<TDocument> filter1 = filterDefinition;
            UpdateDefinition<TDocument> update = updateDefinition;
            CancellationToken cancellationToken1 = cancellationToken;
            return collection.UpdateOneAsync(filter1, update, cancellationToken: cancellationToken1);
        }

        public virtual Task<UpdateResult> UpdateOneAsync<TDocument, TField>(
            FilterDefinition<TDocument> filterDefinition,
            Expression<Func<TDocument, TField>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            IMongoCollection<TDocument> collection = this.GetCollection<TDocument>();
            UpdateDefinition<TDocument> updateDefinition = Builders<TDocument>.Update.Set(field, value);
            FilterDefinition<TDocument> filter = filterDefinition;
            UpdateDefinition<TDocument> update = updateDefinition;
            CancellationToken cancellationToken1 = cancellationToken;
            return collection.UpdateOneAsync(filter, update, cancellationToken: cancellationToken1);
        }

        public virtual Task<BulkWriteResult<TDocument>> UpdateManyAsync<TDocument>(
            IEnumerable<TDocument> documents,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            IMongoCollection<TDocument> collection = this.GetCollection<TDocument>();
            IList<ReplaceOneModel<TDocument>> list = documents.Select(document =>
            {
                TDocument document1 = document;
                FilterDefinition<TDocument> filterDefinition = Builders<TDocument>.Filter.Eq(x => x.Id, document.Id);
                return new
                {
                    document = document1,
                    filter = filterDefinition
                };
            }).Select(t => new ReplaceOneModel<TDocument>(t.filter, t.document)
            {
                IsUpsert = false
            }).ToList();
            BulkWriteOptions options = new BulkWriteOptions();
            options.IsOrdered = false;
            CancellationToken cancellationToken1 = cancellationToken;
            return collection.BulkWriteAsync(list, options, cancellationToken1);
        }

        public virtual Task<UpdateResult> UpdateManyAsync<TDocument>(
            IEnumerable<TKey> keys,
            UpdateDefinition<TDocument> updateDefinition,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return this.GetCollection<TDocument>().UpdateManyAsync(Builders<TDocument>.Filter.In(x => x.Id, keys), updateDefinition, cancellationToken: cancellationToken);
        }

        public virtual Task<UpdateResult> UpdateManyAsync<TDocument>(
            Expression<Func<TDocument, bool>> filter,
            UpdateDefinition<TDocument> updateDefinition,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return this.GetCollection<TDocument>().UpdateManyAsync(Builders<TDocument>.Filter.Where(filter), updateDefinition, cancellationToken: cancellationToken);
        }

        public virtual Task<UpdateResult> UpdateManyAsync<TDocument, TField>(
            IEnumerable<TKey> keys,
            Expression<Func<TDocument, TField>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return this.GetCollection<TDocument>().UpdateManyAsync(Builders<TDocument>.Filter.In(x => x.Id, keys), Builders<TDocument>.Update.Set(field, value), cancellationToken: cancellationToken);
        }

        public virtual Task<UpdateResult> UpdateManyAsync<TDocument, TField>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, TField>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            IMongoCollection<TDocument> collection = this.GetCollection<TDocument>();
            FilterDefinition<TDocument> filterDefinition = Builders<TDocument>.Filter.Where(filter);
            UpdateDefinition<TDocument> updateDefinition = Builders<TDocument>.Update.Set(field, value);
            FilterDefinition<TDocument> filter1 = filterDefinition;
            UpdateDefinition<TDocument> update = updateDefinition;
            CancellationToken cancellationToken1 = cancellationToken;
            return collection.UpdateManyAsync(filter1, update, cancellationToken: cancellationToken1);
        }

        public virtual Task<UpdateResult> UpdateManyAsync<TDocument, TField>(
            FilterDefinition<TDocument> filterDefinition,
            Expression<Func<TDocument, TField>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            IMongoCollection<TDocument> collection = this.GetCollection<TDocument>();
            UpdateDefinition<TDocument> updateDefinition = Builders<TDocument>.Update.Set(field, value);
            FilterDefinition<TDocument> filter = filterDefinition;
            UpdateDefinition<TDocument> update = updateDefinition;
            CancellationToken cancellationToken1 = cancellationToken;
            return collection.UpdateManyAsync(filter, update, cancellationToken: cancellationToken1);
        }

        public virtual Task<UpdateResult> IncrementFieldAsync<TDocument, TField>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, TField>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            IMongoCollection<TDocument> collection = this.GetCollection<TDocument>();
            FilterDefinition<TDocument> filterDefinition = Builders<TDocument>.Filter.Where(filter);
            UpdateDefinition<TDocument> updateDefinition = Builders<TDocument>.Update.Inc(field, value);
            FilterDefinition<TDocument> filter1 = filterDefinition;
            UpdateDefinition<TDocument> update = updateDefinition;
            CancellationToken cancellationToken1 = cancellationToken;
            return collection.UpdateOneAsync(filter1, update, cancellationToken: cancellationToken1);
        }

        public virtual Task<UpdateResult> IncrementFieldAsync<TDocument, TField>(
            TKey key,
            Expression<Func<TDocument, TField>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            return this.GetCollection<TDocument>().UpdateOneAsync(Builders<TDocument>.Filter.Eq(x => x.Id, key), Builders<TDocument>.Update.Inc(field, value), cancellationToken: cancellationToken);
        }

        public virtual Task<TDocument> IncrementFieldAndGetAsync<TDocument, TField>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, TField>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            IMongoCollection<TDocument> collection = this.GetCollection<TDocument>();
            FilterDefinition<TDocument> filterDefinition = Builders<TDocument>.Filter.Where(filter);
            UpdateDefinition<TDocument> updateDefinition = Builders<TDocument>.Update.Inc(field, value);
            FilterDefinition<TDocument> filter1 = filterDefinition;
            UpdateDefinition<TDocument> update = updateDefinition;
            FindOneAndUpdateOptions<TDocument, TDocument> options = new FindOneAndUpdateOptions<TDocument, TDocument>();
            options.ReturnDocument = ReturnDocument.After;
            CancellationToken cancellationToken1 = cancellationToken;
            return collection.FindOneAndUpdateAsync(filter1, update, options, cancellationToken1);
        }

        public virtual Task<TDocument> IncrementFieldAndGetAsync<TDocument, TField>(
            TKey key,
            Expression<Func<TDocument, TField>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            IMongoCollection<TDocument> collection = this.GetCollection<TDocument>();
            FilterDefinition<TDocument> filterDefinition = Builders<TDocument>.Filter.Eq(x => x.Id, key);
            UpdateDefinition<TDocument> updateDefinition = Builders<TDocument>.Update.Inc(field, value);
            IMongoCollection<TDocument> mongoCollection = collection;
            FilterDefinition<TDocument> filter = filterDefinition;
            UpdateDefinition<TDocument> update = updateDefinition;
            FindOneAndUpdateOptions<TDocument, TDocument> options = new FindOneAndUpdateOptions<TDocument, TDocument>();
            options.ReturnDocument = ReturnDocument.After;
            CancellationToken cancellationToken1 = cancellationToken;
            return mongoCollection.FindOneAndUpdateAsync(filter, update, options, cancellationToken1);
        }

        public virtual Task<UpdateResult> AddToSetAsync<TDocument, TField>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, IEnumerable<TField>>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            IMongoCollection<TDocument> collection = this.GetCollection<TDocument>();
            FilterDefinition<TDocument> filterDefinition = Builders<TDocument>.Filter.Where(filter);
            UpdateDefinition<TDocument> set = Builders<TDocument>.Update.AddToSet(field, value);
            FilterDefinition<TDocument> filter1 = filterDefinition;
            UpdateDefinition<TDocument> update = set;
            CancellationToken cancellationToken1 = cancellationToken;
            return collection.UpdateOneAsync(filter1, update, cancellationToken: cancellationToken1);
        }

        public virtual Task<UpdateResult> RemoveFromSetAsync<TDocument, TField>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, IEnumerable<TField>>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            IMongoCollection<TDocument> collection = this.GetCollection<TDocument>();
            FilterDefinition<TDocument> filterDefinition = Builders<TDocument>.Filter.Where(filter);
            UpdateDefinition<TDocument> updateDefinition = Builders<TDocument>.Update.Pull(field, value);
            FilterDefinition<TDocument> filter1 = filterDefinition;
            UpdateDefinition<TDocument> update = updateDefinition;
            CancellationToken cancellationToken1 = cancellationToken;
            return collection.UpdateOneAsync(filter1, update, cancellationToken: cancellationToken1);
        }

        public virtual Task<UpdateResult> RemoveFromSetAsync<TDocument, TField>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, IEnumerable<TField>>> field,
            Expression<Func<TField, bool>> condition,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            IMongoCollection<TDocument> collection = this.GetCollection<TDocument>();
            FilterDefinition<TDocument> filterDefinition = Builders<TDocument>.Filter.Where(filter);
            FilterDefinition<TField> filter1 = Builders<TField>.Filter.Where(condition);
            UpdateDefinition<TDocument> updateDefinition = Builders<TDocument>.Update.PullFilter(field, filter1);
            FilterDefinition<TDocument> filter2 = filterDefinition;
            UpdateDefinition<TDocument> update = updateDefinition;
            CancellationToken cancellationToken1 = cancellationToken;
            return collection.UpdateOneAsync(filter2, update, cancellationToken: cancellationToken1);
        }

        public virtual Task<ReplaceOneResult> UpsertOneAsync<TDocument>(
            TDocument document,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            IMongoCollection<TDocument> collection = this.GetCollection<TDocument>();
            FilterDefinition<TDocument> filterDefinition = Builders<TDocument>.Filter.Eq(x => x.Id, document.Id);
            IMongoCollection<TDocument> mongoCollection = collection;
            FilterDefinition<TDocument> filter = filterDefinition;
            TDocument replacement = document;
            UpdateOptions options = new UpdateOptions();
            options.IsUpsert = true;
            CancellationToken cancellationToken1 = cancellationToken;
            return mongoCollection.ReplaceOneAsync(filter, replacement, options, cancellationToken1);
        }

        public virtual Task<BulkWriteResult<TDocument>> UpsertManyAsync<TDocument>(
            IEnumerable<TDocument> documents,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>
        {
            IMongoCollection<TDocument> collection = this.GetCollection<TDocument>();
            IList<ReplaceOneModel<TDocument>> list = documents.Select(document =>
            {
                TDocument document1 = document;
                FilterDefinition<TDocument> filterDefinition = Builders<TDocument>.Filter.Eq(x => x.Id, document.Id);
                return new
                {
                    document = document1,
                    filterDefinition = filterDefinition
                };
            }).Select(t => new ReplaceOneModel<TDocument>(t.filterDefinition, t.document)
            {
                IsUpsert = true
            }).ToList();
            BulkWriteOptions options = new BulkWriteOptions();
            options.IsOrdered = false;
            CancellationToken cancellationToken1 = cancellationToken;
            return collection.BulkWriteAsync(list, options, cancellationToken1);
        }
    }
}