﻿using System;
using System.Diagnostics.CodeAnalysis;
using Data.Mongo.Data;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.Serializers;

namespace Data.Mongo.Repository.Abstracts
{
    public static class MongoRepositoryExtension
    {
        public static IServiceCollection AddMongoRepository(
            this IServiceCollection services,
            Action<MongoContextConfig> configureOptions)
        {
            services.Configure(configureOptions);
            services.AddMongoRepository();
            return services;
        }

        public static IServiceCollection AddMongoRepository(
            this IServiceCollection services)
        {
            services.AddSingleton<IMongoContext, MongoContext>();
            ServiceCollectionServiceExtensions.AddSingleton(services, typeof(IMongoRepository<>), typeof(MongoRepository<>));
            BsonDefaults.GuidRepresentation = new MongoConfig().GuidRepresentation;
            return services;
        }

        public static IServiceCollection ConfigureMongo(
            this IServiceCollection services,
            Action<MongoConfig> configureOptions)
        {
            MongoConfig mongoConfig = new MongoConfig();
            configureOptions(mongoConfig);
            if (mongoConfig.IgnoreExtraElements)
                ConventionRegistry.Register("IgnoreExtraElements", new ConventionPack()
                {
                    new IgnoreExtraElementsConvention(true)
                }, type => true);
            if (mongoConfig.UseDecimals)
                MongoRepositoryExtension.ConfigureDecimal();
            switch (mongoConfig.DateTimeOffsetRepresentation)
            {
                case DateTimeOffsetRepresentation.Array:
                    MongoRepositoryExtension.ConfigureDateTimeOffset(BsonType.Array);
                    break;
                case DateTimeOffsetRepresentation.Document:
                    MongoRepositoryExtension.ConfigureDateTimeOffset(BsonType.Document);
                    break;
                case DateTimeOffsetRepresentation.String:
                    MongoRepositoryExtension.ConfigureDateTimeOffset(BsonType.String);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            BsonDefaults.GuidRepresentation = mongoConfig.GuidRepresentation;
            return services;

        }
        

        private static void ConfigureDecimal()
        {
            BsonSerializer.RegisterSerializer(typeof(Decimal), new DecimalSerializer(BsonType.Decimal128));
            BsonSerializer.RegisterSerializer(typeof(Decimal?), new NullableSerializer<Decimal>(new DecimalSerializer(BsonType.Decimal128)));
        }

        private static void ConfigureDateTimeOffset(BsonType bsonType)
        {
            BsonSerializer.RegisterSerializer(typeof(DateTimeOffset), new DateTimeOffsetSerializer(bsonType));
            BsonSerializer.RegisterSerializer(typeof(DateTimeOffset?), new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(bsonType)));
        }
    }

    public enum DateTimeOffsetRepresentation
    {
        Array,
        Document,
        String,
    }

    [ExcludeFromCodeCoverage]
    public class MongoConfig
    {
        public bool UseDecimals { get; set; }

        public bool IgnoreExtraElements { get; set; }

        public DateTimeOffsetRepresentation DateTimeOffsetRepresentation { get; set; }

        public GuidRepresentation GuidRepresentation { get; set; } = GuidRepresentation.Standard;
    }
}