﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Data.Mongo.Data;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace Data.Mongo.Repository.Abstracts
{
    public interface IMongoRepository<TKey> where TKey : IEquatable<TKey>
    {
        Task<DeleteResult> DeleteOneAsync<TDocument>(
            TKey key,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<DeleteResult> DeleteOneAsync<TDocument>(
            TDocument document,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<DeleteResult> DeleteOneAsync<TDocument>(
            Expression<Func<TDocument, bool>> filter,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<DeleteResult> DeleteManyAsync<TDocument>(
            Expression<Func<TDocument, bool>> filter,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<DeleteResult> DeleteManyAsync<TDocument>(
            IEnumerable<TDocument> documents,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<DeleteResult> DeleteManyAsync<TDocument>(
            IEnumerable<TKey> keys,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task AddOneAsync<TDocument>(TDocument document, CancellationToken cancellationToken = default) where TDocument : IDocument<TKey>;

        Task AddManyAsync<TDocument>(
            IEnumerable<TDocument> documents,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task AddManyOrderedAsync<TDocument>(
            IEnumerable<TDocument> documents,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        IMongoCollection<TDocument> GetCollection<TDocument>() where TDocument : IDocument<TKey>;

        Task<TDocument> GetByKeyAsync<TDocument>(TKey key, CancellationToken cancellationToken = default) where TDocument : IDocument<TKey>;

        Task<TDocument> GetOneAsync<TDocument>(
            Expression<Func<TDocument, bool>> filter,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<TDocument> GetOneAsync<TDocument>(
            FilterDefinition<TDocument> filterDefinition,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        IFindFluent<TDocument, TDocument> Find<TDocument>(
            Expression<Func<TDocument, bool>> filter)
            where TDocument : IDocument<TKey>;

        Task<bool> AnyAsync<TDocument>(
            Expression<Func<TDocument, bool>> filter,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<IList<TDocument>> GetManyAsync<TDocument>(
            Expression<Func<TDocument, bool>> filter,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<IList<TDocument>> GetManyAsync<TDocument>(
            FilterDefinition<TDocument> filterDefinition,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<IList<TDocument>> GetManyAsync<TDocument, TField>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, TField>> fieldSelector,
            IEnumerable<TField> values,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<long> CountAsync<TDocument>(
            Expression<Func<TDocument, bool>> filter,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<TProjection> ProjectOneAsync<TDocument, TProjection>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, TProjection>> projection,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<IList<TProjection>> ProjectManyAsync<TDocument, TProjection>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, TProjection>> projection,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<IList<TDocument>> GetPageAsync<TDocument>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, object>> sortSelector,
            bool ascending = true,
            int skip = 0,
            int take = 50,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<IList<TDocument>> GetPageAsync<TDocument>(
            Expression<Func<TDocument, bool>> filter,
            SortDefinition<TDocument> sortDefinition,
            int skip = 0,
            int take = 50,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        IMongoQueryable<TDocument> GetQuery<TDocument>() where TDocument : IDocument<TKey>;

        Task<ReplaceOneResult> UpdateOneAsync<TDocument>(
            TDocument document,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<UpdateResult> UpdateOneAsync<TDocument>(
            TKey key,
            UpdateDefinition<TDocument> updateDefinition,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<UpdateResult> UpdateOneAsync<TDocument>(
            Expression<Func<TDocument, bool>> filter,
            UpdateDefinition<TDocument> updateDefinition,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<UpdateResult> UpdateOneAsync<TDocument, TField>(
            TKey key,
            Expression<Func<TDocument, TField>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<UpdateResult> UpdateOneAsync<TDocument, TField>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, TField>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<UpdateResult> UpdateOneAsync<TDocument, TField>(
            FilterDefinition<TDocument> filterDefinition,
            Expression<Func<TDocument, TField>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<BulkWriteResult<TDocument>> UpdateManyAsync<TDocument>(
            IEnumerable<TDocument> documents,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<UpdateResult> UpdateManyAsync<TDocument>(
            IEnumerable<TKey> keys,
            UpdateDefinition<TDocument> updateDefinition,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<UpdateResult> UpdateManyAsync<TDocument>(
            Expression<Func<TDocument, bool>> filter,
            UpdateDefinition<TDocument> updateDefinition,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<UpdateResult> UpdateManyAsync<TDocument, TField>(
            IEnumerable<TKey> keys,
            Expression<Func<TDocument, TField>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<UpdateResult> UpdateManyAsync<TDocument, TField>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, TField>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<UpdateResult> UpdateManyAsync<TDocument, TField>(
            FilterDefinition<TDocument> filterDefinition,
            Expression<Func<TDocument, TField>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<UpdateResult> IncrementFieldAsync<TDocument, TField>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, TField>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<UpdateResult> IncrementFieldAsync<TDocument, TField>(
            TKey key,
            Expression<Func<TDocument, TField>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<TDocument> IncrementFieldAndGetAsync<TDocument, TField>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, TField>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<TDocument> IncrementFieldAndGetAsync<TDocument, TField>(
            TKey key,
            Expression<Func<TDocument, TField>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<UpdateResult> AddToSetAsync<TDocument, TField>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, IEnumerable<TField>>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<UpdateResult> RemoveFromSetAsync<TDocument, TField>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, IEnumerable<TField>>> field,
            TField value,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<UpdateResult> RemoveFromSetAsync<TDocument, TField>(
            Expression<Func<TDocument, bool>> filter,
            Expression<Func<TDocument, IEnumerable<TField>>> field,
            Expression<Func<TField, bool>> condition,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<ReplaceOneResult> UpsertOneAsync<TDocument>(
            TDocument document,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;

        Task<BulkWriteResult<TDocument>> UpsertManyAsync<TDocument>(
            IEnumerable<TDocument> documents,
            CancellationToken cancellationToken = default)
            where TDocument : IDocument<TKey>;
    }
}