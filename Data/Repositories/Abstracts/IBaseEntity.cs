﻿using System;
using System.Linq;

namespace Data.Repositories.Abstracts
{
    public interface IBaseEntity: IBaseEntity<Guid>
    {
    }

    public interface IBaseEntity<TKey>
    {
        public TKey Id { get; set; }
    }

    public static class BaseEntityExtension
    {
        public static IQueryable<T> FilterById<T>(this IQueryable<T> data, Guid id)
            where T: class, IBaseEntity
        {
            return data.Where(x => x.Id == id);
        }
    }
}