﻿using System.Linq;

namespace Data.Repositories.Abstracts
{
    public interface IMemberRelatedEntity
    {
        public ulong MemberId { get; set; }
    }

    public static class MemberRelatedEntityExtension
    {
        public static IQueryable<T> FilterByMemberId<T>(this IQueryable<T> data, ulong memberId)
            where T : class, IMemberRelatedEntity, IBaseEntity
        {
            return data
                .Where(x => x.MemberId == memberId);
        }
    }
}