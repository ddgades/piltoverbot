﻿using System.Linq;

namespace Data.Repositories.Abstracts
{
    public interface INamedEntity
    {
        public string Name { get; set; }
    }

    public static class NamedEntityExtension
    {
        public static IQueryable<T> FilterByName<T>(this IQueryable<T> data, string name)
            where T: class, INamedEntity, IBaseEntity
        {
            return data
                .Where(x => x.Name == name);
        }
    }
}