﻿using System.Linq;

namespace Data.Repositories.Abstracts
{
    public interface IGuildRelatedEntity
    {
        public ulong GuildId { get; set; }
    }

    public static class GuildRelatedEntityExtension
    {
        public static IQueryable<T> FilterByGuild<T>(this IQueryable<T> data, ulong guildId)
            where T : class, IGuildRelatedEntity, IBaseEntity
        {
            return data
                .Where(x => x.GuildId == guildId);
        }
    }
}