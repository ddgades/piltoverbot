﻿using Data.Repositories.Abstracts;

namespace Data.Entity
{
    public class SummonerAccount: INamedEntity
    {
        public bool IsConfirmed { get; set; }

        public string Name { get; set; }

        public string SummonerId { get; set; }

        public string Puuid { get; set; }

        public string ConfirmationCode { get; set; }
    }
}