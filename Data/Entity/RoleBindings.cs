﻿using System;
using Data.Repositories.Abstracts;

namespace Data.Entity
{
    public class RoleBinding:IBaseEntity, INamedEntity
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Stack { get; set; }

        public ulong RoleId { get; set; }

        public ulong EmojiId { get; set; }
    }
}