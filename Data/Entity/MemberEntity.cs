﻿using System;
using Data.Repositories.Abstracts;

namespace Data.Entity
{
    public class MemberEntity: IBaseEntity, INamedEntity
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public ulong DiscordMemberId { get; set; }

        public Guid GuildId { get; set; }
    }
}