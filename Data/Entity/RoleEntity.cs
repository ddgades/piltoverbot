﻿using System;
using Data.Repositories.Abstracts;

namespace Data.Entity
{
    public class RoleEntity : IBaseEntity, INamedEntity
    {
        public Guid Id { get;set; }
        public string Name { get; set; }
        public Guid RoleBindingId { get; set; }
    }
}