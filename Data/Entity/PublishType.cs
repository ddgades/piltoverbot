﻿using System.Collections.Generic;
using Data.Repositories.Abstracts;

namespace Data.Entity
{
    public class PublishType: INamedEntity
    {
        public string Name { get; set; }

        public virtual List<PublishChannelBinding> ChannelBindings { get; set; }
    }
}