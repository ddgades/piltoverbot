﻿using Data.Repositories.Abstracts;

namespace Data.Entity
{
    public class PublishChannelBinding: INamedEntity
    {
        public string Name { get; set; }
        
        public ulong ApprovalChannelId { get; set; }

        public ulong PublishChannelId { get; set; }
    }
}