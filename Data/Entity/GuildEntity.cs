﻿using System;
using Data.Repositories.Abstracts;

namespace Data.Entity
{
    public class GuildEntity: IBaseEntity, INamedEntity, IGuildRelatedEntity
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public ulong GuildId { get; set; }
    }
}