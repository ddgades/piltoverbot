﻿namespace Data.Entity
{
    public class BulletinBoardConfiguration
    {
        public ulong CategoryId { get; set; }

        public ulong HostRoleId { get; set; }

        public ulong BulletinBoardChannelId { get; set; }

        public ulong DefaultChannelId { get; set; }
    }
}